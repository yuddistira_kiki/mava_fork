package sgo.mobile.mava;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

public class AppHelper {
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public static Point screenSize(Context ctx)
	{
		WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
        if (android.os.Build.VERSION.SDK_INT >= 13) {
        	display.getSize(size);
        } else {
        	size.x = display.getWidth();
        	size.y = display.getHeight();
        }
        return size;
	}

    public static String getUserId(Context context){
        return context.getSharedPreferences("userinfo", Context.MODE_PRIVATE).getString("user_id", "");
    }

    public static String getUserName(Context context){
        return context.getSharedPreferences("userinfo", Context.MODE_PRIVATE).getString("user_name", "");
    }

    public static String getUserEmail(Context context){
        return context.getSharedPreferences("userinfo", Context.MODE_PRIVATE).getString("user_email", "");
    }

    public static String getUserPhone(Context context){
        return context.getSharedPreferences("userinfo", Context.MODE_PRIVATE).getString("user_phone", "");
    }

    public static String getCustomerId(Context context){
        return context.getSharedPreferences("userinfo", Context.MODE_PRIVATE).getString("cust_id", "");
    }

    public static String getUrlSgoPlus(Context context){
        return context.getSharedPreferences("userinfo", Context.MODE_PRIVATE).getString("rel_sgo_plus", "");
    }

    public static String getUserType(Context context){
        return context.getSharedPreferences("userinfo", Context.MODE_PRIVATE).getString("user_type", "");
    }

}

