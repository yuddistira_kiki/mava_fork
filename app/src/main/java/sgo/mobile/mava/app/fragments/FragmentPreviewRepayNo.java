package sgo.mobile.mava.app.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.securepreferences.SecurePreferences;

import org.json.JSONException;
import org.json.JSONObject;

import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.dialogs.DefinedDialog;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

/**
 * Created by thinkpad on 1/26/2016.
 */
public class FragmentPreviewRepayNo extends Fragment {
    View view;
    SecurePreferences sp;
    Activity activity;

    ProgressDialog out;
    AlertDialog alert;

    LinearLayout layout_token;
    EditText et_token;
    TextView tv_tx_id, tv_comm_name, tv_bank_name, tv_bank_product, tv_ccy_id, tv_amount, tv_admin_fee;
    Button btnProcess, btnCancel;

    String comm_id, comm_name, comm_code, bank_code, product_code,
            tx_id, bank_name, product_name, ccy_id, amount, admin_fee;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view  = inflater.inflate(R.layout.fragment_preview_repay_no, container, false);

        activity = getActivity();
        out = DefinedDialog.CreateProgressDialog(activity, null);

        sp = new SecurePreferences(activity);

        layout_token = (LinearLayout) view.findViewById(R.id.layout_token);
        et_token = (EditText) view.findViewById(R.id.token_value);
        tv_tx_id = (TextView) view.findViewById(R.id.tx_id_value);
        tv_comm_name = (TextView) view.findViewById(R.id.comm_name_value);
        tv_bank_name = (TextView) view.findViewById(R.id.bank_name_value);
        tv_bank_product = (TextView) view.findViewById(R.id.bank_product_value);
        tv_ccy_id = (TextView) view.findViewById(R.id.ccy_id_value);
        tv_amount = (TextView) view.findViewById(R.id.amount_value);
        tv_admin_fee = (TextView) view.findViewById(R.id.admin_fee_value);
        btnProcess = (Button) view.findViewById(R.id.preview_btn_process);
        btnCancel = (Button) view.findViewById(R.id.preview_btn_cancel);

        Bundle bundle = getArguments();
        if(bundle != null) {
            tx_id = bundle.getString(AppParams.TX_ID);
            bank_code = bundle.getString(AppParams.BANK_CODE);
            bank_name = bundle.getString(AppParams.BANK_NAME);
            product_code = bundle.getString(AppParams.PRODUCT_CODE);
            product_name = bundle.getString(AppParams.PRODUCT_NAME);

            comm_id = bundle.getString(AppParams.COMM_ID);
            comm_name = bundle.getString(AppParams.COMM_NAME);
            comm_code = bundle.getString(AppParams.COMM_CODE);
            ccy_id = bundle.getString(AppParams.CCY_ID);
            amount = bundle.getString(AppParams.AMOUNT);
            admin_fee = bundle.getString(AppParams.ADMIN_FEE);

            tv_tx_id.setText(tx_id);
            tv_comm_name.setText(comm_name);
            tv_bank_name.setText(bank_name);
            tv_bank_product.setText(product_name);
            tv_ccy_id.setText(ccy_id);
            tv_amount.setText(amount);
            tv_admin_fee.setText(admin_fee);

            getToken();
        }

        btnProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnProcess.setEnabled(false);
                btnProcess.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        btnProcess.setEnabled(true);
                    }
                }, 3000);

                if (inputTokenValidation()) {
                    confirmToken(et_token.getText().toString());
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                backToPayment();
            }
        });

        return view;
    }


    public void getToken(){
        try {
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());

            RequestParams params = new RequestParams();
            params.put(AppParams.COMM_CODE, comm_code);
            params.put(AppParams.TX_ID, tx_id);
            params.put(AppParams.PRODUCT_CODE, product_code);

            Log.d("get token params", params.toString());
            client.post(AplConstants.InquiryTrxAPI, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("get token response:", content);
                    try {
                        out.dismiss();
                        JSONObject json = new JSONObject(content);

                        String error_code = json.getString("error_code");
                        String error_msg = json.getString("error_message");

                        if (error_code.equals(AppParams.SUCCESS_CODE)) {
                            //berhasil kirim token
                            layout_token.setVisibility(View.VISIBLE);
                        }
                        else {
                            AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());
                            dialog.setTitle("Alert");
                            dialog.setMessage(error_msg);
                            dialog.setCancelable(false);
                            dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
//                                    dialog.dismiss();
                                    backToPayment();
                                }
                            });
                            alert = dialog.create();
                            alert.show();
                        }
                    }
                    catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                };

                public void onFailure(Throwable error, String content) {
                    Toast.makeText(activity, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            });
        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }

    public void confirmToken(String token){
        try {
            out.show();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());

            RequestParams params = new RequestParams();
            params.put(AppParams.COMM_ID, comm_id);
            params.put(AppParams.TX_ID, tx_id);
//            params.put(AppParams.COMM_CODE, comm_code_sp);
            params.put(AppParams.COMM_CODE, comm_code);
            params.put(AppParams.PRODUCT_CODE, product_code);
            params.put(AppParams.PRODUCT_VALUE, token);
            params.put(AppParams.MEMBER_ID, "");

            Log.d("confirm token params", params.toString());
            client.post(AplConstants.InsertTrxAPI, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("confirm token response:", content);
                    try {
                        out.dismiss();
                        JSONObject json = new JSONObject(content);

                        String error_code = json.getString("error_code");
                        String error_msg = json.getString("error_message");

                        if (error_code.equals(AppParams.SUCCESS_CODE)) {
                            //berhasil confirm token
                            backToCommunity();
                        }
                        else if(error_code.equals("0031")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                            builder.setTitle("Alert");
                            builder.setMessage(error_msg);
                            builder.setCancelable(false);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    backToPayment();
                                }
                            });
                            alert = builder.create();
                            alert.show();
                        }
                        else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                            builder.setTitle("Alert");
                            builder.setMessage(error_msg);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            alert = builder.create();
                            alert.show();
                        }
                    }
                    catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                };

                public void onFailure(Throwable error, String content) {
                    Toast.makeText(activity, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            });
        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }


    public void backToPayment(){
        hideKeyboard();
        Fragment newFragment = new FragmentPaymentRepayment();
        Bundle args = new Bundle();
        args.putString(AppParams.COMM_ID, comm_id);
        args.putString(AppParams.COMM_CODE, comm_code);
        args.putString(AppParams.COMM_NAME, comm_name);
        newFragment.setArguments(args);
        switchFragment(newFragment);
    }

    public void backToCommunity(){
        hideKeyboard();
        Fragment newFragment = new FragmentCommunityRepayment();
        Bundle bundle = new Bundle();
        bundle.putString("title", getResources().getString(R.string.kmk));
        newFragment.setArguments(bundle);
        switchFragment(newFragment);
    }

    public boolean inputTokenValidation(){
        if(et_token.getText().toString().length()==0){
            et_token.requestFocus();
            et_token.setError(this.getString(R.string.validation_otp));
            return false;
        }
        return true;
    }

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

    public void hideKeyboard(){
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
