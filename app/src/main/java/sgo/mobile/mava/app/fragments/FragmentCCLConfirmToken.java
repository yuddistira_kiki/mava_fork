package sgo.mobile.mava.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.ui.dialog.DefinedDialog;
import sgo.mobile.mava.frameworks.math.FormatCurrency;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

import java.util.Arrays;

import static sgo.mobile.mava.frameworks.array.ArrayUtils.jsonStringToArray;

public class FragmentCCLConfirmToken extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;
    String Result;

    public String transfer_code;
    public String amount, shop_code, tx_remark, bank_code, bank_name, product_code, ccy_id, no_phone_source, benef_acct_no, benef_acct_name, tx_fee, fee_acct_no, charges_acct_no, buyer_fee, seller_fee, commission_fee, commission_acct_no, bbs_product, resend_token;
    public String comm_id, comm_name, comm_code, buss_scheme_code;
    public String member_id, member_code;
    public String Rq_trx_id, Rq_date, Rq_desc;
    private String source_acct_no, source_acct_name, payment_id, payment_amount, shop_id, payment_ref_tx, payment_ref_buyer, payment_ref_seller, payment_ref_commission, payment_status, payment_remark, payment_created;

    private String flag_continue = "Y";

    EditText txtOtp;
    Button btnDone;
    Button btnCancel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ccl_confirmtoken, container, false);

        txtOtp                = (EditText) view.findViewById(R.id.txtOtp);
        btnDone               = (Button) view.findViewById(R.id.btnDone);

        Bundle bundle         = this.getArguments();
        comm_id               = bundle.getString("comm_id");
        comm_name             = bundle.getString("comm_name");
        comm_code             = bundle.getString("comm_code");
        buss_scheme_code      = bundle.getString("buss_scheme_code");
        member_code           = bundle.getString("member_code");
        member_id             = bundle.getString("member_id");
        bank_code             = bundle.getString("bank_code");
        bank_name             = bundle.getString("bank_name");
        product_code          = bundle.getString("product_code");
        ccy_id                = bundle.getString("ccy_id");
        amount                = bundle.getString("amount");
        shop_code             = bundle.getString("shop_code");
        shop_id               = bundle.getString("shop_id");
        tx_remark             = bundle.getString("tx_remark");
        no_phone_source       = bundle.getString("no_phone_source");
        benef_acct_no         = bundle.getString("benef_acct_no");
        benef_acct_name       = bundle.getString("benef_acct_name");
        fee_acct_no           = bundle.getString("fee_acct_no");
        charges_acct_no       = bundle.getString("charges_acct_no");
        buyer_fee             = bundle.getString("buyer_fee");
        buyer_fee             = ((buyer_fee != null) && !buyer_fee.isEmpty()) ? buyer_fee : "0";
        seller_fee            = bundle.getString("seller_fee");
        tx_fee                = bundle.getString("tx_fee");
        commission_fee        = bundle.getString("commission_fee");
        commission_fee        = ((commission_fee != null) && !commission_fee.isEmpty()) ? commission_fee : "0";
        commission_acct_no    = bundle.getString("commission_acct_no");
        bbs_product           = bundle.getString("bbs_product_json");
        resend_token          = bundle.getString("resend_token");

        Rq_trx_id            = bundle.getString("Rq_trx_id");
        Rq_date              = bundle.getString("Rq_date");
        Rq_desc              = bundle.getString("Rq_desc");


        TextView lbl_id_transaksi = (TextView) view.findViewById(R.id.lbl_id_transaksi);
        lbl_id_transaksi.setText(Rq_trx_id);

        TextView lbl_community = (TextView) view.findViewById(R.id.lbl_community);
        lbl_community.setText(comm_name);

        TextView lbl_member = (TextView) view.findViewById(R.id.lbl_member);
        lbl_member.setText(member_code);

        TextView lbl_bank = (TextView) view.findViewById(R.id.lbl_bank);
        lbl_bank.setText(bank_name);

        TextView lbl_shop_code = (TextView) view.findViewById(R.id.lbl_shop_code);
        lbl_shop_code.setText(shop_code);

        TextView lbl_jumlah = (TextView) view.findViewById(R.id.lbl_jumlah);
        lbl_jumlah.setText(FormatCurrency.getRupiahFormat(amount));

        TextView lbl_pesan = (TextView) view.findViewById(R.id.lbl_pesan);
        lbl_pesan.setText(tx_remark);

        TextView lbl_fee = (TextView) view.findViewById(R.id.lbl_fee);

        Double dbl_comm_fee = Double.parseDouble(commission_fee);
        int int_comm_fee    = dbl_comm_fee.intValue();

        final int fee_total   = int_comm_fee + Integer.parseInt(buyer_fee);
        lbl_fee.setText(FormatCurrency.getRupiahFormat(Integer.toString(fee_total)));

        TextView lbl_total = (TextView) view.findViewById(R.id.lbl_total);
        final int total_amount = Integer.parseInt(amount) + fee_total;
        lbl_total.setText(FormatCurrency.getRupiahFormat(Integer.toString(total_amount)));

        btnDone.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                transfer_code        = txtOtp.getText().toString();
                if(transfer_code.equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                }else{

                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Memproses Token CCL...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                    RequestParams params   = new RequestParams();

                    params.put("trx_id", Rq_trx_id);
                    params.put("token_id", transfer_code);
                    params.put("flag_reversal", flag_continue);

                    Log.d("params", params.toString());
                    client.post(AplConstants.ConfirmTokenCCLMobileAPI, params, new AsyncHttpResponseHandler() {
                        public void onSuccess(String content) {
                            Log.d("result:", content);
                            try {
                                JSONObject object         = new JSONObject(content);

                                String error_code         = object.getString("error_code");
                                String error_msg          = object.getString("error_message");

                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }

                                if (error_code.equals(AppParams.SUCCESS_CODE)) {

                                    source_acct_no   = object.getString("source_acct_no");
                                    source_acct_name = object.getString("source_acct_name");

                                    JSONArray arrDataConfirmToken = object.getJSONArray("data");

                                    if(arrDataConfirmToken.length() > 0){
                                        payment_id               = arrDataConfirmToken.getJSONObject(0).getString("trx_id");
                                        payment_amount           = arrDataConfirmToken.getJSONObject(0).getString("trx_amount");
                                        payment_ref_tx           = arrDataConfirmToken.getJSONObject(0).getString("external_trx");
                                        payment_ref_buyer        = arrDataConfirmToken.getJSONObject(0).getString("external_buyer");
                                        payment_ref_seller       = arrDataConfirmToken.getJSONObject(0).getString("external_seller");
                                        payment_ref_commission   = arrDataConfirmToken.getJSONObject(0).getString("external_commission");
                                        payment_status           = arrDataConfirmToken.getJSONObject(0).getString("trx_status");
                                        payment_remark           = arrDataConfirmToken.getJSONObject(0).getString("trx_reason");
                                        payment_created          = arrDataConfirmToken.getJSONObject(0).getString("trx_date");
                                    }else{
                                        payment_id               = "";
                                        payment_amount           = "";
                                        payment_ref_tx           = "";
                                        payment_ref_buyer        = "";
                                        payment_ref_seller       = "";
                                        payment_ref_commission   = "";
                                        payment_status           = "";
                                        payment_remark           = "";
                                        payment_created          = "";
                                    }

                                    if(payment_status.equalsIgnoreCase("S")) {

                                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                        alert.setTitle("Cash Collection");
                                        alert.setMessage("Proses Konfirmasi Token CCL Berhasil");
                                        alert.setPositiveButton("OK", null);
                                        alert.show();

                                        AsyncHttpClient client = new AsyncHttpClient();
                                        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                                        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                                        RequestParams params = new RequestParams();

                                        params.put("member_id", member_id);
                                        params.put("shop_id", shop_id);
                                        params.put("tx_remark", tx_remark);
                                        params.put("source_acct_no", source_acct_no);
                                        params.put("source_acct_name", source_acct_name);
                                        params.put("bank_code", bank_code);
                                        params.put("product_code", product_code);
                                        params.put("ccy_id", ccy_id);

                                        params.put("payment_id", payment_id);
                                        params.put("amount", payment_amount);
                                        params.put("payment_ref_tx", payment_ref_tx);
                                        params.put("payment_ref_buyer", payment_ref_buyer);
                                        params.put("payment_ref_seller", payment_ref_seller);
                                        params.put("payment_ref_commission", payment_ref_commission);
                                        params.put("payment_status", payment_status);
                                        params.put("payment_remark", payment_remark);
                                        params.put("payment_created", payment_created);

                                        params.put("seller_fee", seller_fee);
                                        params.put("buyer_fee", buyer_fee);
                                        params.put("tx_fee", tx_fee);
                                        params.put("benef_acct_no", benef_acct_no);
                                        params.put("benef_acct_name", benef_acct_name);
                                        params.put("charges_acct_no", charges_acct_no);
                                        params.put("fee_acct_no", fee_acct_no);
                                        params.put("commission_fee", commission_fee);
                                        params.put("commission_acct_no", commission_acct_no);

                                        try{
                                            JSONArray mJSONArray = new JSONArray(Arrays.asList(jsonStringToArray(bbs_product)));
                                            params.put("bbs_product", mJSONArray.toString());
                                        } catch (JSONException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                            params.put("bbs_product", "");
                                        }

                                        Log.d("params ccl payment report ", params.toString());
                                        client.post(AplConstants.PaymentReportCCLMobileAPI, params, new AsyncHttpResponseHandler() {
                                            public void onSuccess(String content) {
                                                Log.d("result payment report :", content);
                                                try {
                                                    JSONObject object = new JSONObject(content);

                                                    String error_code = object.getString("error_code");
                                                    String error_msg = object.getString("error_message");

                                                    if (error_code.equals(AppParams.SUCCESS_CODE)) {

                                                        Fragment newFragment = null;
                                                        newFragment = new FragmentCCLDeposit();
                                                        Bundle args = new Bundle();
                                                        args.putString("comm_id", comm_id);
                                                        args.putString("comm_name", comm_name);
                                                        args.putString("comm_code", comm_code);
                                                        args.putString("buss_scheme_code", buss_scheme_code);
                                                        args.putString("member_id", member_id);
                                                        args.putString("member_code", member_code);
                                                        newFragment.setArguments(args);
                                                        switchFragment(newFragment);

                                                    } else {
                                                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                                        alert.setTitle("Cash Collection");
                                                        alert.setMessage("Cash Collection : " + error_msg);
                                                        alert.setPositiveButton("OK", null);
                                                        alert.show();
                                                    }

                                                } catch (JSONException e) {
                                                    // TODO Auto-generated catch block
                                                    e.printStackTrace();
                                                }
                                            }

                                            ;

                                            public void onFailure(Throwable error, String content) {
                                                if (pDialog != null) {
                                                    pDialog.dismiss();
                                                }
                                                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                                            }

                                            ;
                                        });
                                    }else {
                                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                        alert.setTitle("Cash Collection");
                                        alert.setMessage("Cash Collection : " + payment_remark);
                                        alert.setPositiveButton("OK", null);
                                        alert.show();
                                    }
                                } else {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Cash Collection");
                                    alert.setMessage("Cash Collection : " + error_msg);
                                    alert.setPositiveButton("OK", null);
                                    alert.show();
                                }

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        };

                        public void onFailure(Throwable error, String content) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        btnCancel               = (Button) view.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Fragment newFragment = null;
                newFragment = new FragmentCCLDeposit();
                Bundle args = new Bundle();
                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("buss_scheme_code",buss_scheme_code);
                args.putString("member_id", member_id);
                args.putString("member_code", member_code);
                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });


        return view;
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

}
