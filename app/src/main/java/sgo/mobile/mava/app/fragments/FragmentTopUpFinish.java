package sgo.mobile.mava.app.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;

public class FragmentTopUpFinish extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;
    String Result;

    String account_no, account_name, total;
    String rq_trx_id, rq_desc;

    Button btnDone;

    private String  member_id, member_code, tx_id,tx_date, currency, amount, remark, bank_code, bank_name, product_code, product_name, comm_code, comm_name, benef_acct_no, benef_acct_name, tx_amount, buyer_fee, seller_fee, tx_fee, charges_acct_no, fee_acct_no;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_topup_finish, container, false);

        Bundle bundle         = this.getArguments();

        tx_id                 = bundle.getString("PaymentId");
        account_no            = bundle.getString("BenefAccNo");
        account_name          = bundle.getString("BenefAccName");

        member_id               = bundle.getString("MemberId");
        member_code             = bundle.getString("MemberCode");
        tx_id                   = bundle.getString("PaymentId");
        bank_code               = bundle.getString("BankCode");
        bank_name               = bundle.getString("BankName");
        product_code            = bundle.getString("ProductCode");
        product_name            = bundle.getString("ProductName");
        comm_code               = bundle.getString("CommCode");
        comm_name               = bundle.getString("CommName");
        benef_acct_no           = bundle.getString("BenefAccNo");
        benef_acct_name         = bundle.getString("BenefAccName");
        amount                  = bundle.getString("Amount");
        buyer_fee               = bundle.getString("BuyerFee");
        seller_fee              = bundle.getString("SellerFee");
        tx_fee                  = bundle.getString("TxFee");
        charges_acct_no         = bundle.getString("ChargesAcctNo");
        fee_acct_no             = bundle.getString("FeeAcctNo");
        total                   = bundle.getString("Total");
        remark                  = bundle.getString("Remark");

        TextView lbl_community =(TextView) view.findViewById(R.id.lbl_community);
        lbl_community.setText(comm_name);

        TextView lbl_member =(TextView) view.findViewById(R.id.lbl_member);
        lbl_member.setText(member_code);

        TextView lbl_bank =(TextView) view.findViewById(R.id.lbl_bank);
        lbl_bank.setText(bank_name);

        TextView lbl_bank_product =(TextView) view.findViewById(R.id.lbl_bank_product);
        lbl_bank_product.setText(product_name);


        TextView lbl_account_no =(TextView) view.findViewById(R.id.lbl_account_no);
        lbl_account_no.setText(account_no);

        TextView lbl_account_name =(TextView) view.findViewById(R.id.lbl_account_name);
        lbl_account_name.setText(account_name);

        TextView lbl_jumlah         = (TextView) view.findViewById(R.id.lbl_jumlah);
        lbl_jumlah.setText(amount);

        TextView lbl_buyer_fee    = (TextView) view.findViewById(R.id.lbl_buyer_fee);
        lbl_buyer_fee.setText(buyer_fee);

        TextView lbl_seller_fee    = (TextView) view.findViewById(R.id.lbl_seller_fee);
        lbl_seller_fee.setText(seller_fee);

        TextView lbl_message    = (TextView) view.findViewById(R.id.lbl_message);
        lbl_message.setText(remark);

        TextView lbl_jumlah_total    = (TextView) view.findViewById(R.id.lbl_jumlah_total);
        lbl_jumlah_total.setText(total);


        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                Fragment newFragment = null;
                newFragment = new FragmentTopUpPrincipal();
                switchFragment(newFragment);
            }
        });

        return view;
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }
}
