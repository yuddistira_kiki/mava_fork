package sgo.mobile.mava.app.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.MySSLSocketFactory;

import org.json.JSONException;
import org.json.JSONObject;

import sgo.mobile.mava.BuildConfig;
import sgo.mobile.mava.R;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.conf.DateTimeFormat;
import sgo.mobile.mava.conf.InetHandler;
import sgo.mobile.mava.conf.SMSclass;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

/**
 * Created by yuddistirakiki on 5/19/16.
 */
public class SMSDialog extends Dialog {

    private ImageView img_view;
    private ProgressBar progBar;
    private TextView progText,tvMessage;
    private Button btnOk,btnCancel;
    private CountDownTimer cdTimer;
    private static int lenghtTimer; //5 minute
    private final static int intervalTimer = 1000;
    private final static int max_fail_connect = 5; //5 minute
    private static Boolean isStop;
    private String imeiDevice,ICCIDDevice;
    SMSclass smsClass;
    String message1, timeStamp, comm_id;
    SMSclass.SMS_VERIFY_LISTENER smsVerifyListener;
    Handler handler;
    private int idx_fail;


    public interface DialogButtonListener{
        void onClickOkButton(View v, boolean isLongClick);
        void onClickCancelButton(View v, boolean isLongClick);
        void onSuccess(int user_is_new);
        void onSuccess(String product_value);
    }

    private DialogButtonListener deListener;

    public SMSDialog(Context context, String comm_id, DialogButtonListener _listener) {
        super(context);
        this.deListener = _listener;
        this.comm_id = comm_id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCanceledOnTouchOutside(false);
        // Include dialog.xml file
        setContentView(R.layout.dialog_sms_notification);

        if(BuildConfig.FLAVOR.equals("development"))
            lenghtTimer = 150000;
        else
            lenghtTimer = 300000;

        smsClass = new SMSclass(getContext());
        // set values for custom dialog components - text, image and button

        img_view = (ImageView)findViewById(R.id.dialog_pic_msg);
        progBar = (ProgressBar)findViewById(R.id.dialog_probar_inquiry);
        progText = (TextView)findViewById(R.id.dialog_duration_inquiry);
        tvMessage = (TextView)findViewById(R.id.message_dialog);

        progBar.setMax(100);

        message1 = getContext().getString(R.string.appname) + " " + getContext().getString(R.string.dialog_sms_msg);

        btnOk = (Button) findViewById(R.id.btn_dialog_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(InetHandler.isNetworkAvailable(getContext())){
                    progBar.setProgress(0);
                    tvMessage.setText(getContext().getString(R.string.dialog_sms_msg2));
                    progBar.setVisibility(View.VISIBLE);
                    progText.setVisibility(View.VISIBLE);
                    btnOk.setVisibility(View.GONE);
                    btnCancel.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        img_view.setImageDrawable(getContext().getDrawable(R.drawable.phone_sms_icon_process));
                    } else {
                        img_view.setImageDrawable(getContext().getResources().getDrawable(R.drawable.phone_sms_icon_process));
                    }

                    isStop = false;
                    idx_fail = 0;
                    sentInquirySMS();
                    cdTimer.start();
                    if (deListener != null)
                        deListener.onClickOkButton(v, false);
                }
                else {
                    Toast.makeText(getContext(),getContext().getString(R.string.inethandler_dialog_message),Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnCancel = (Button) findViewById(R.id.btn_dialog_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                tvMessage.setText(message1);
                progText.setVisibility(View.GONE);
                progBar.setVisibility(View.GONE);
                btnOk.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    img_view.setImageDrawable(getContext().getDrawable(R.drawable.phone_sms_icon));
                }
                else {
                    img_view.setImageDrawable(getContext().getResources().getDrawable(R.drawable.phone_sms_icon_process));
                }
                cdTimer.cancel();
//                MyApiClient.CancelRequestWS(getContext(),true);
                isStop = true;
                dismiss();
                if (deListener!=null)
                    deListener.onClickCancelButton(v,false);
            }
        });


        tvMessage.setText(message1);


        cdTimer = new CountDownTimer(lenghtTimer,intervalTimer) {
            int i = 0;
            @Override
            public void onTick(long millisUntilFinished) {
                progText.setText(DateTimeFormat.convertMilisToMinute(millisUntilFinished));
                i++;
                int prog = (i*100000)/lenghtTimer;

                progBar.setProgress(prog);
            }

            @Override
            public void onFinish() {
                isStop = true;
//                MyApiClient.CancelRequestWS(getContext(),true);

                i = 0;
                tvMessage.setText(getContext().getString(R.string.dialog_sms_msg3));
                progText.setVisibility(View.GONE);
                progBar.setVisibility(View.GONE);
                btnOk.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    img_view.setImageDrawable(getContext().getDrawable(R.drawable.phone_sms_icon_fail));
                }
                else {
                    img_view.setImageDrawable(getContext().getResources().getDrawable(R.drawable.phone_sms_icon_fail));
                }
            }
        };

        SMSclass test = new SMSclass(getContext());
        imeiDevice = test.getDeviceIMEI();
        ICCIDDevice = test.getDeviceICCID();
        Log.d("smsdialog", "device imei/ICCID : " + imeiDevice +"/"+ ICCIDDevice);

        smsVerifyListener = new SMSclass.SMS_VERIFY_LISTENER() {
            @Override
            public void success() {
                Log.d("smsdialog", "sms terkirim sukses");
                smsClass.Close();
            }

            @Override
            public void failed() {
                Log.d("smsdialog", "sms terkirim gagal");
                tvMessage.setText(getContext().getString(R.string.dialog_sms_msg3));
                progText.setVisibility(View.GONE);
                progBar.setVisibility(View.GONE);
                btnOk.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    img_view.setImageDrawable(getContext().getDrawable(R.drawable.phone_sms_icon_fail));
                }
                else {
                    img_view.setImageDrawable(getContext().getResources().getDrawable(R.drawable.phone_sms_icon_fail));
                }
                cdTimer.cancel();
//                MyApiClient.CancelRequestWS(getContext(),true);
                isStop = true;
                smsClass.Close();
            }
        };
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        timeStamp = String.valueOf(DateTimeFormat.getCurrentDateTimeMillis());
        Log.d("smsdialog", "isi timestamp : "+timeStamp);
    }

    public void reset(){
        tvMessage.setText(message1);
        progText.setVisibility(View.GONE);
        progBar.setVisibility(View.GONE);
        btnOk.setVisibility(View.VISIBLE);
        btnCancel.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            img_view.setImageDrawable(getContext().getDrawable(R.drawable.phone_sms_icon));
        }
        else {
            img_view.setImageDrawable(getContext().getResources().getDrawable(R.drawable.phone_sms_icon));
        }
        DestroyDialog();
    }

    public void DestroyDialog(){
        if(cdTimer != null) {
            isStop = true;
//            idx_fail = 0;
            cdTimer.cancel();
        }
    }

    public void sentSms(String comm_id){
        if(!isStop) {
            Log.d("smsdialog", "jalanin sentSMSVerify");
            smsClass.sendSMSVerify(comm_id, AplConstants.INCOMINGSMS_SPRINT, imeiDevice, ICCIDDevice, timeStamp, smsVerifyListener);
        }
    }

    private void sentInquirySMS (){
        try{
            Log.d("smsdialog", "idx fail = "+String.valueOf(idx_fail));
            if(idx_fail <= max_fail_connect && InetHandler.isNetworkAvailable(getContext())) {
                if(!isStop) {
                    try {
                        AsyncHttpClient client = new AsyncHttpClient();
                        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                        client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);
                        RequestParams params = new RequestParams();

//                        String param_sales_id = AppHelper.getUserId(getContext());
                        params.put(AppParams.COMM_ID, comm_id);
                        params.put(AppParams.IMEI, imeiDevice);
                        params.put(AppParams.ICCID, ICCIDDevice);
                        params.put(AppParams.SENT, timeStamp);

                        Log.d("params", params.toString());
                        client.post(AplConstants.LINK_INQUIRY_SMS, params, new AsyncHttpResponseHandler() {

                            public void onSuccess(String content) {
                                Log.d("result:", content);
                                try {

                                    // parse json data and store into arraylist variables
                                    final JSONObject json = new JSONObject(content);
                                    //Toast.makeText(getActivity(), content, Toast.LENGTH_LONG).show();
                                    String error_code = json.getString("error_code");
                                    String error_message = json.getString("error_message");

                                    if (handler == null)
                                        handler = new Handler();

                                    if (error_code.equals(AppParams.SUCCESS_CODE)) {
                                        isStop = true;
                                        tvMessage.setText(getContext().getString(R.string.dialog_sms_msg4));
                                        progText.setVisibility(View.GONE);
                                        progBar.setVisibility(View.GONE);
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                            img_view.setImageDrawable(getContext().getDrawable(R.drawable.phone_sms_icon_success));
                                        } else {
                                            img_view.setImageDrawable(getContext().getResources().getDrawable(R.drawable.phone_sms_icon_success));
                                        }
                                        cdTimer.cancel();

                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                saveData(json);
                                            }
                                        }, 2000);

                                    } else {
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                sentInquirySMS();
                                            }
                                        }, 3000);
                                    }

                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();

                                }
                            }

                            public void onFailure(Throwable error, String content) {
                                Log.d("smsdialog", "Error Koneksi login:" + error.toString());
                                if (handler == null)
                                    handler = new Handler();

                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        idx_fail++;
                                        sentInquirySMS();
                                    }
                                }, 3000);

                                Toast.makeText(getContext(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                            }
                        });
                    } catch (Exception e) {

                    }
                }
            }else {
                tvMessage.setText(getContext().getString(R.string.dialog_sms_msg3));
                progText.setVisibility(View.GONE);
                progBar.setVisibility(View.GONE);
                btnOk.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    img_view.setImageDrawable(getContext().getDrawable(R.drawable.phone_sms_icon_fail));
                }
                else {
                    img_view.setImageDrawable(getContext().getResources().getDrawable(R.drawable.phone_sms_icon_fail));
                }
//            MyApiClient.CancelRequestWS(getContext(),true);
                DestroyDialog();
                idx_fail = 0;
            }
        }catch (Exception e){
            Log.d("smsdialog", "httpclient:"+e.getMessage());
        }
    }

    private void saveData(JSONObject mObj){
        deListener.onSuccess(mObj.optString("sender_id",""));
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }
}
