package sgo.mobile.mava.app.ui.dialog;

import android.app.ProgressDialog;
import android.content.Context;

public class DefinedDialog {
    public static ProgressDialog CreateProgressDialog(Context context,
                                                      ProgressDialog dialog, String message) {
        dialog = new ProgressDialog(context);
        dialog.setMessage(message);
        //dialog.setTitle(title);
        dialog.setIndeterminate(true);
        dialog.show();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }
}