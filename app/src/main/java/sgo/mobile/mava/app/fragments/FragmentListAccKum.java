package sgo.mobile.mava.app.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.adapter.AccountKumAdapter;
import sgo.mobile.mava.app.beans.AccountKumBean;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

/**
 * Created by thinkpad on 3/29/2016.
 */
public class FragmentListAccKum extends Fragment {

    ProgressBar prgLoading;
    TextView txtAlert;
    TextView lbl_header;
    Activity act;

    ListView lvAccKum;
    AccountKumAdapter accountKumAdapter;

    ArrayList<AccountKumBean> listAccKum;

    String cust_id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        act = getActivity();
        View view = inflater.inflate(R.layout.fragment_list_acc_kum, container, false);

        Bundle bundle = this.getArguments();
        if(bundle != null) {
            if (bundle.containsKey(AppParams.MEMBER_CUST_ID))
                cust_id = bundle.getString(AppParams.MEMBER_CUST_ID, "");
        }
        else {
            cust_id = AppHelper.getUserId(act);
        }

        txtAlert = (TextView) view.findViewById(R.id.txtAlert);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        lbl_header = (TextView) view.findViewById(R.id.label_header);
        lvAccKum = (ListView) view.findViewById(R.id.listAccKum);

        listAccKum = new ArrayList<>();
        accountKumAdapter = new AccountKumAdapter(act, listAccKum);
        lvAccKum.setAdapter(accountKumAdapter);

        getListAccKum();

        // event listener to handle list when clicked
        lvAccKum.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {

                Fragment newFragment = null;
                newFragment = new FragmentAccKumDetail();
                Bundle args = new Bundle();
                args.putString(AppParams.ACCOUNT_NO, listAccKum.get(position).getAcct_no());
                args.putString(AppParams.BANK_CODE, listAccKum.get(position).getBank_code());
                args.putString(AppParams.BANK_NAME, listAccKum.get(position).getBank_name());
                args.putString(AppParams.ACCOUNT_NAME, listAccKum.get(position).getAcct_name());
                args.putString(AppParams.MEMBER_CUST_ID, cust_id);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        return view;
    }

    public void getListAccKum() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);

        RequestParams params = new RequestParams();
        params.put("cust_id", cust_id);

        Log.d("params", params.toString());
        client.post(AplConstants.AcctMandiriKUM, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {
                Log.d("result:", content);
                try {
                    // parse json data and store into arraylist variables
                    JSONObject json = new JSONObject(content);
                    String error_code         = json.getString("error_code");
                    String error_message      = json.getString("error_message");

                    if (error_code.equals(AppParams.SUCCESS_CODE)) {
                        JSONArray arr_bank_data = json.getJSONArray("bank_data"); // this is the "items: [ ] part
                        JSONObject bank_data = arr_bank_data.getJSONObject(0);
                        String bank_code = bank_data.getString("bank_code");
                        String bank_name = bank_data.getString("bank_name");

                        JSONArray arrAccKum = bank_data.getJSONArray("account");

                        for (int i = 0; i < arrAccKum.length(); i++) {
                            JSONObject object = arrAccKum.getJSONObject(i);

                            AccountKumBean accountKumBean = new AccountKumBean();
                            accountKumBean.setBank_code(bank_code);
                            accountKumBean.setBank_name(bank_name);
                            accountKumBean.setAcct_no(object.getString("acct_no"));
                            accountKumBean.setAcct_name(object.getString("acct_name"));
                            accountKumBean.setCcy_id(object.getString("ccy_id"));
                            listAccKum.add(accountKumBean);
                        }

                        // when finish parsing, hide progressbar
                        prgLoading.setVisibility(View.GONE);

                        // if data available show data on list
                        // otherwise, show alert text
                        if(listAccKum.size() > 0){
                            lvAccKum.setVisibility(View.VISIBLE);
                            lvAccKum.setAdapter(accountKumAdapter);
                            lbl_header.setVisibility(View.VISIBLE);
                        }else{
                            txtAlert.setVisibility(View.VISIBLE);
                        }

                    }else{
                        prgLoading.setVisibility(View.GONE);
                        txtAlert.setVisibility(View.VISIBLE);
                        txtAlert.setText(error_message);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            }

            public void onFailure(Throwable error, String content) {
                Toast.makeText(act, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }
}
