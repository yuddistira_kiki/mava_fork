package sgo.mobile.mava.app.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.beans.OperatorsBean;

import java.util.ArrayList;

public class OperatorAdapter extends ArrayAdapter<OperatorsBean> {
    private Activity context;
    ArrayList<OperatorsBean> data = null;

    public OperatorAdapter(Activity context, int resource,
                       ArrayList<OperatorsBean> data) {
        super(context, resource, data);
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            row = inflater.inflate(R.layout.simple_spinner_item, parent, false);
        }

        OperatorsBean item = data.get(position);

        if (item != null) { // Parse the data from each object and set it.
            TextView MemberName = (TextView) row.findViewById(R.id.item_value);
            if (MemberName != null) {
                MemberName.setText(item.getName());
            }
        }
        return row;
    }
}