package sgo.mobile.mava.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.adapter.InvoicePayPaymentAdapter;
import sgo.mobile.mava.app.beans.InvoicePayPaymentBean;
import sgo.mobile.mava.app.ui.dialog.DefinedDialog;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.math.FormatCurrency;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class FragmentInvoicePayPayment extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;

    public String member_id, member_code, member_name, session_id_param;
    public String total_pay;
    public String session_id_var;
    public String validate_token;
    public String comm_id, comm_name, comm_code, sales_alias,buss_scheme_code;
    public static String bank_code, bank_name, ccy_id, product_code, seller_fee, buyer_fee, tx_fee, member_phone, benef_acct_no, benef_acct_name, charges_acct_no, fee_acct_no, flag_continue;
    private String source_acct_no, source_acct_name;
    private String token_data, data_cftoken, data_inv;

    // declare view objects
    View view;
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert;
    TextView lbl_header;
    TableLayout tabel_footer;
    TextView lbl_total_pay_amount;

    InvoicePayPaymentAdapter invoicePayPaymentAdapter;
    ArrayList<InvoicePayPaymentBean> arrInvoicePay;

    // create arraylist variables to store data from server
    private String partial_payment = "";
//    public static ArrayList<String> doc_no_pay = new ArrayList<String>();
//    public static ArrayList<String> doc_id_pay = new ArrayList<String>();
//    public static ArrayList<String> amount_pay = new ArrayList<String>();
//    public static ArrayList<String> remain_amount_pay = new ArrayList<String>();
//    public static ArrayList<String> input_amount_pay = new ArrayList<String>();
//    public static ArrayList<String> ccy_pay = new ArrayList<String>();
//    public static ArrayList<String> doc_desc_pay = new ArrayList<String>();
//    public static ArrayList<String> session_id_pay = new ArrayList<String>();

    EditText txtOtp;
    Button btnDone;
    Button btnCancel;

    // create price format
    DecimalFormat formatData = new DecimalFormat("#.##");

    String lineSep = System.getProperty("line.separator");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view  = inflater.inflate(R.layout.fragment_invpayment_confirm, container, false);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        tabel_footer = (TableLayout) view.findViewById(R.id.tabel_footer);
        lbl_total_pay_amount = (TextView) view.findViewById(R.id.lbl_total_pay_amount);

        txtOtp                = (EditText) view.findViewById(R.id.txtOtp);
        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnCancel             = (Button) view.findViewById(R.id.btnCancel);

        arrInvoicePay = new ArrayList<InvoicePayPaymentBean>();

        Bundle bundle         = this.getArguments();
        member_id             = bundle.getString("member_id");
        member_code           = bundle.getString("member_code");
        member_name           = bundle.getString("member_name");
        session_id_param      = bundle.getString("session_id_param");
        session_id_param      = (session_id_param.equalsIgnoreCase("null")) ? "" : session_id_param;
        ccy_id                = bundle.getString("ccy_id");

        comm_id               = bundle.getString("comm_id");
        comm_name             = bundle.getString("comm_name");
        comm_code             = bundle.getString("comm_code");
        sales_alias           = bundle.getString("sales_alias");
        buss_scheme_code      = bundle.getString("buss_scheme_code");

        bank_code              = bundle.getString("bank_code");
        bank_name              = bundle.getString("bank_name");
        product_code           = bundle.getString("product_code");
        ccy_id                 = bundle.getString("ccy_id");
        seller_fee             = bundle.getString("seller_fee");
        buyer_fee              = bundle.getString("buyer_fee");
        tx_fee                 = bundle.getString("tx_fee");
        member_phone           = bundle.getString("member_phone");
        benef_acct_no          = bundle.getString("benef_acct_no");
        benef_acct_name        = bundle.getString("benef_acct_name");
        charges_acct_no        = bundle.getString("charges_acct_no");
        fee_acct_no            = bundle.getString("fee_acct_no");

        partial_payment       = bundle.getString("partial_payment");
        flag_continue         = bundle.getString("flag_continue");


        TextView lbl_bank = (TextView) view.findViewById(R.id.lbl_bank_name);
        lbl_bank.setText(bank_name);


        lbl_header = (TextView) view.findViewById(R.id.label_header);
        lbl_header.setText(member_name);

        invoicePayPaymentAdapter = new InvoicePayPaymentAdapter(getActivity().getApplicationContext(), arrInvoicePay);
        listMenu.setAdapter(invoicePayPaymentAdapter);
        parseJSONData();

        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                validate_token        = txtOtp.getText().toString();
                if(validate_token.equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                }else{

                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Memproses Token...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                    RequestParams params   = new RequestParams();

                    params.put("data_inv", token_data);
                    params.put("token_id", validate_token);
                    params.put("flag_reversal", flag_continue);

                    Log.d("params", params.toString());
                    client.post(AplConstants.ConfirmTokenInvPaymentMobileAPI, params, new AsyncHttpResponseHandler() {
                        public void onSuccess(String content) {
                            Log.d("result:", content);
                            try {
                                JSONObject object         = new JSONObject(content);

                                String error_code         = object.getString("error_code");
                                String error_msg          = object.getString("error_message");

                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }

                                if (error_code.equals(AppParams.SUCCESS_CODE)) {

                                    source_acct_no   = object.getString("source_acct_no");
                                    source_acct_name = object.getString("source_acct_name");

                                    JSONArray token_data_arr = object.getJSONArray("data");
                                    data_cftoken             = token_data_arr.toString();

                                    String confirmMessage = "";
                                    for (int i = 0; i < token_data_arr.length(); i++) {
                                        JSONObject data = token_data_arr.getJSONObject(i);
                                        if(data.getString("trx_status").equalsIgnoreCase("S")){
                                            String trx_reason_s = data.getString("trx_reason");
                                            String trx_id_s =  data.getString("trx_id");
                                            String trx_status_s = data.getString("trx_status");
                                            confirmMessage = confirmMessage + "Trx ID : " + trx_id_s + lineSep;
                                            confirmMessage = confirmMessage + "Status : sukses" + lineSep;
                                            confirmMessage = confirmMessage + lineSep;
                                        }else{
                                            String trx_reason_f = data.getString("trx_reason");
                                            String trx_id_f = data.getString("trx_id");
                                            String trx_status_f = data.getString("trx_status");
                                            confirmMessage = confirmMessage + "Trx ID : " + trx_id_f + lineSep;
                                            confirmMessage = confirmMessage + "Status : Gagal" + lineSep;
                                            confirmMessage = confirmMessage + "Keterangan : Gagal" + trx_reason_f + lineSep;
                                            confirmMessage = confirmMessage + lineSep;
                                        }
                                    }

                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Konfirmasi Token Invoice Payment");
                                    alert.setMessage("Invoice Payment : " + lineSep + confirmMessage);
                                    alert.setPositiveButton("OK", null);
                                    alert.show();

                                    AsyncHttpClient client = new AsyncHttpClient();
                                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                                    RequestParams params   = new RequestParams();

                                    params.put("member_id", member_id);
                                    String param_sales_id = AppHelper.getUserId(getActivity());
                                    params.put("sales_id", param_sales_id);
                                    params.put("source_acct_no", source_acct_no);
                                    params.put("source_acct_name", source_acct_name);

                                    params.put("bank_code", bank_code);
                                    params.put("product_code", product_code);
                                    params.put("ccy_id", ccy_id);
                                    params.put("seller_fee", seller_fee);
                                    params.put("buyer_fee", buyer_fee);
                                    params.put("tx_fee", tx_fee);
                                    params.put("benef_acct_no", benef_acct_no);
                                    params.put("benef_acct_name", benef_acct_name);
                                    params.put("charges_acct_no", charges_acct_no);
                                    params.put("fee_acct_no", fee_acct_no);

                                    params.put("data_cftoken", data_cftoken);
                                    params.put("session_id", session_id_param);

                                    client.post(AplConstants.PaymentReportInvPaymentMobileAPI, params, new AsyncHttpResponseHandler() {
                                        public void onSuccess(String content) {
                                            Log.d("result payment report :", content);
                                            try {
                                                JSONObject object         = new JSONObject(content);

                                                String error_code         = object.getString("error_code");
                                                String error_msg          = object.getString("error_message");

                                                if (error_code.equals(AppParams.SUCCESS_CODE)) {
                                                    hideKeyboard();

                                                    Fragment newFragment = null;
                                                    newFragment = new FragmentMemberInvPayment();
                                                    Bundle args = new Bundle();
                                                    args.putString("comm_id", comm_id);
                                                    args.putString("comm_name", comm_name);
                                                    args.putString("comm_code", comm_code);
                                                    args.putString("sales_alias", sales_alias);
                                                    args.putString("buss_scheme_code", buss_scheme_code);

                                                    newFragment.setArguments(args);
                                                    switchFragment(newFragment);

                                                }else{
                                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                                    alert.setTitle("Invoice Payment");
                                                    alert.setMessage("Invoice Payment : " + error_msg);
                                                    alert.setPositiveButton("OK", null);
                                                    alert.show();
                                                }

                                            } catch (JSONException e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                            }
                                        };

                                        public void onFailure(Throwable error, String content) {
                                            if (pDialog != null) {
                                                pDialog.dismiss();
                                            }
                                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                                        }
                                    });

                                } else {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Keterangan Token Invoice Payment");
                                    alert.setMessage("Invoice Payment : " + error_msg);
                                    alert.setPositiveButton("OK", null);
                                    alert.show();
                                }

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        };

                        public void onFailure(Throwable error, String content) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                hideKeyboard();
                Fragment newFragment = null;
                newFragment = new FragmentInvoiceListPayment();
                Bundle args = new Bundle();
                args.putString("member_id", member_id);
                args.putString("member_code", member_code);
                args.putString("member_name", member_name);
                args.putString("session_id_param", session_id_param);

                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("sales_alias", sales_alias);
                args.putString("buss_scheme_code", buss_scheme_code);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });


        return view;
    }

    public void parseJSONData(){
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);
        RequestParams params = new RequestParams();

        String param_sales_id = AppHelper.getUserId(getActivity());
        params.put("member_id", member_id);
        params.put("sales_id", param_sales_id);
        params.put("session_id", session_id_param);
        params.put("ccy_id", ccy_id);
        params.put("buyer_fee", buyer_fee);

        params.put("ccy_id", ccy_id);
        params.put("no_phone_source", member_phone);
        params.put("benef_acct_no", benef_acct_no);
        params.put("fee_acct_no", fee_acct_no);
        params.put("charges_acct_no", charges_acct_no);
        params.put("buyer_fee", buyer_fee);
        params.put("seller_fee", seller_fee);
        params.put("commission_fee", "0");
        params.put("commission_acct_no", "");
        params.put("resend_token", "N");

        Log.d("params", params.toString());
        client.post(AplConstants.InvListPayMobileAPI, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {
                Log.d("result:", content);
                try {
                    // parse json data and store into arraylist variables
                    JSONObject json = new JSONObject(content);

                    String error_code         = json.getString("error_code");
                    String error_msg          = json.getString("error_message");

                    if (error_code.equals(AppParams.SUCCESS_CODE)) {

                        JSONArray token_data_arr = json.getJSONArray("token_data");
                        token_data               = token_data_arr.toString();

                        JSONArray data = json.getJSONArray("invoice_data"); // this is the "items: [ ] part

                        partial_payment = json.getString("partial_payment");
                        total_pay       = json.getString("total_pay");
                        session_id_var  = json.getString("session_id_var");

                        String total_payment = FormatCurrency.getRupiahFormat(total_pay);
                        lbl_total_pay_amount.setText(total_payment);

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject object = data.getJSONObject(i);

                            InvoicePayPaymentBean invoicePayPaymentBean = new InvoicePayPaymentBean();
                            invoicePayPaymentBean.setDoc_no_pay(object.getString("doc_no"));
                            invoicePayPaymentBean.setDoc_id_pay(object.getString("doc_id"));
                            invoicePayPaymentBean.setAmount_pay(object.getString("amount"));
                            invoicePayPaymentBean.setRemain_amount_pay(object.getString("remain_amount"));
                            invoicePayPaymentBean.setCcy_pay(object.getString("ccy"));
                            invoicePayPaymentBean.setDoc_desc_pay(object.getString("doc_desc"));
                            invoicePayPaymentBean.setInput_amount_pay(object.getString("input_amount"));
                            invoicePayPaymentBean.setSession_id_pay(object.getString("session_id"));

                            arrInvoicePay.add(invoicePayPaymentBean);
                        }

                        // when finish parsing, hide progressbar
                        prgLoading.setVisibility(8);

                        // if data available show data on list
                        // otherwise, show alert text
                        if(arrInvoicePay.size() > 0){
                            listMenu.setVisibility(0);
                            invoicePayPaymentAdapter.notifyDataSetChanged();
                            lbl_header.setVisibility(0);
                            tabel_footer.setVisibility(0);
                        }else{
                            txtAlert.setVisibility(0);
                        }

                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(view.getContext());
                        alert.setTitle("Keterangan Token Invoice Payment");
                        alert.setMessage("Invoice Payment : " + error_msg);
                        alert.setPositiveButton("OK", null);
                        alert.show();

                        Fragment newFragment = null;
                        newFragment = new FragmentInvoiceListPayment();
                        Bundle args = new Bundle();
                        args.putString("member_id", member_id);
                        args.putString("member_code", member_code);
                        args.putString("member_name", member_name);
                        args.putString("session_id_param", session_id_param);

                        args.putString("comm_id", comm_id);
                        args.putString("comm_name", comm_name);
                        args.putString("comm_code", comm_code);
                        args.putString("sales_alias", sales_alias);
                        args.putString("buss_scheme_code", buss_scheme_code);

                        newFragment.setArguments(args);
                        switchFragment(newFragment);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            };

            public void onFailure(Throwable error, String content) {
                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }
        });
    }

    // clear arraylist variables before used
//    void clearData(){
//        doc_no_pay.clear();
//        doc_id_pay.clear();
//        amount_pay.clear();
//        remain_amount_pay.clear();
//        ccy_pay.clear();
//        doc_desc_pay.clear();
//        input_amount_pay.clear();
//        session_id_pay.clear();
//    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }


    public void hideKeyboard(){
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}