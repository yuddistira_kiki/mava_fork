package sgo.mobile.mava.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.fragments.FragmentInvoiceListPayment;
import sgo.mobile.mava.frameworks.math.FormatCurrency;

import java.util.StringTokenizer;

public class InvoicePaymentAdapter extends BaseAdapter {
    private Activity activity;

    public InvoicePaymentAdapter(Activity act) {
        this.activity = act;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentInvoiceListPayment.doc_no.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_invdgi_list_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        String remain = FragmentInvoiceListPayment.remain_amount.get(position);

        holder.txtText    = (TextView) convertView.findViewById(R.id.txtText);
        holder.txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);
        holder.txtSubText2 = (TextView) convertView.findViewById(R.id.txtSubText2);

        holder.txtText.setText("Invoice " + FragmentInvoiceListPayment.doc_no.get(position));
        holder.txtSubText.setText( "Sisa : " +  FormatCurrency.getRupiahFormat(remain));

        String InputAmount = FragmentInvoiceListPayment.input_amount.get(position);
        if (InputAmount != null && !InputAmount.equals("") && !InputAmount.equals("null") && Long.parseLong(InputAmount) > 0){
            String buyer_fee      = FragmentInvoiceListPayment.buyer_fee;
            buyer_fee             = (buyer_fee != null && !buyer_fee.equals("") && !buyer_fee.equals("null") && Long.parseLong(buyer_fee) > 0)? buyer_fee : "0";
            long total_pay_plus_fee     = Long.parseLong(InputAmount) + Long.parseLong(buyer_fee);
            String total_pay_fee_string  = Long.toString(total_pay_plus_fee);
            StringTokenizer tokens = new StringTokenizer(total_pay_fee_string, ".");
            String first = tokens.nextToken();

            holder.txtSubText2.setVisibility(View.VISIBLE);
            holder.txtSubText2.setText( "Bayar : " +  FormatCurrency.getRupiahFormat(first) + " (Fee : " + FormatCurrency.getRupiahFormat(buyer_fee) + ")");
        }else{
            holder.txtSubText2.setVisibility(View.GONE);
        }

        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText, txtSubText2;
    }
}
