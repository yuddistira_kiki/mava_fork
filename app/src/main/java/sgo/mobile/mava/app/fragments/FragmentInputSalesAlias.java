package sgo.mobile.mava.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.ui.dialog.DefinedDialog;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

public class FragmentInputSalesAlias extends Fragment {

    private ProgressDialog pDialog;
    FragmentManager fm;
    String Result;

    public String comm_id, comm_name, comm_code, buss_scheme_code;
    public String member_id, member_code;
    public String sales_id;


    Button btnDone;
    Button btnCancel;
    EditText txt_sales_id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_input_sales_alias, container, false);

        Bundle bundle         = this.getArguments();
        comm_id               = bundle.getString("comm_id");
        comm_name             = bundle.getString("comm_name");
        comm_code             = bundle.getString("comm_code");
        buss_scheme_code      = bundle.getString("buss_scheme_code");
        member_code           = bundle.getString("member_code");
        member_id             = bundle.getString("member_id");

        txt_sales_id          = (EditText)view.findViewById(R.id.txtSalesID);
        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View arg0) {
                sales_id = txt_sales_id.getText().toString();
                if (sales_id.equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                } else {

                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Processing Sales ID Payment Point...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                    RequestParams params   = new RequestParams();

                    params.put("member_id", member_id);
                    params.put("sales_code", sales_id);
                    Log.d("params", params.toString());
                    client.post(AplConstants.DiCCLListMobileAPI, params, new AsyncHttpResponseHandler() {
                        public void onSuccess(String content) {
                            Log.d("result:", content);
                            try {
                                JSONObject object         = new JSONObject(content);

                                String error_code         = object.getString("error_code");
                                String error_msg          = object.getString("error_message");

                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }

                                if (error_code.equals(AppParams.SUCCESS_CODE)) {

                                    String sales_id_data      = object.getString("sales_id");
                                    String sales_name         = object.getString("sales_name");
                                    String flag_invoice       = object.getString("flag_invoice");

                                    JSONArray array_bank      = object.getJSONArray("bank_data");
                                    String bank_data          = array_bank.toString();

                                    if(flag_invoice.equalsIgnoreCase("R")){

                                        JSONArray array_invoice     = object.getJSONArray("invoice_data");
                                        String invoice_data         = array_invoice.toString();

                                        String total_invoice        = object.getString("total_invoice");

                                        Fragment newFragment = null;
                                        newFragment = new FragmentRandomCCLDI();
                                        Bundle args = new Bundle();
                                        args.putString("comm_id", comm_id);
                                        args.putString("comm_name", comm_name);
                                        args.putString("comm_code", comm_code);
                                        args.putString("buss_scheme_code",buss_scheme_code);

                                        args.putString("member_code", member_code);
                                        args.putString("member_id", member_id);

                                        args.putString("sales_id", sales_id_data);
                                        args.putString("sales_name", sales_name);
                                        args.putString("sales_alias", sales_id);
                                        args.putString("flag_invoice", flag_invoice);

                                        args.putString("bank_data", bank_data);
                                        args.putString("invoice_data", invoice_data);
                                        args.putString("total_invoice", total_invoice);
                                        newFragment.setArguments(args);
                                        switchFragment(newFragment);

                                    }else{

                                        JSONArray array    = object.getJSONArray("amount_data");
                                        String hold_amount = array.getJSONObject(0).getString("hold_amount");
                                        String ccy_id      = array.getJSONObject(0).getString("ccy_id");

                                        Fragment newFragment = null;
                                        newFragment = new FragmentFifoHiloCCLDI();
                                        Bundle args = new Bundle();
                                        args.putString("comm_id", comm_id);
                                        args.putString("comm_name", comm_name);
                                        args.putString("comm_code", comm_code);
                                        args.putString("buss_scheme_code",buss_scheme_code);

                                        args.putString("member_code", member_code);
                                        args.putString("member_id", member_id);

                                        args.putString("sales_id", sales_id_data);
                                        args.putString("sales_alias", sales_id);
                                        args.putString("sales_name", sales_name);
                                        args.putString("flag_invoice", flag_invoice);

                                        args.putString("bank_data", bank_data);
                                        args.putString("hold_amount", hold_amount);
                                        args.putString("ccy_id", ccy_id);

                                        newFragment.setArguments(args);
                                        switchFragment(newFragment);
                                    }
                                } else {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Invoice List Payment Point");
                                    alert.setMessage("Invoice List Payment Point : " + error_msg);
                                    alert.setPositiveButton("OK", null);
                                    alert.show();
                                }
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();

                            }
                        };

                        public void onFailure(Throwable error, String content) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });


        btnCancel               = (Button) view.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Fragment newFragment = null;
                newFragment = new FragmentMemberCCLDI();
                Bundle args = new Bundle();
                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("buss_scheme_code",buss_scheme_code);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });


        return view;
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

}

