package sgo.mobile.mava.app.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.securepreferences.SecurePreferences;

import org.json.JSONException;
import org.json.JSONObject;

import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.dialogs.DefinedDialog;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

/**
 * Created by thinkpad on 1/21/2016.
 */
public class FragmentPreviewRepayYes extends Fragment {
    View view;
    SecurePreferences sp;
    Activity activity;

    ProgressDialog out;
    AlertDialog alert;

    LinearLayout layout_max_partial, layout_input_amount, layout_token;
    EditText et_input_amount, et_token;
    TextView tv_tx_id, tv_comm_name, tv_bank_name, tv_bank_product, tv_ccy_id, tv_amount, tv_admin_fee, tv_desc, tv_max_partial;
    Button btnProcess, btnCancel;

    String comm_id, comm_name, comm_code, bank_code, product_code,
            tx_id, bank_name, product_name, ccy_id, flag_partial;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view  = inflater.inflate(R.layout.fragment_preview_repay_yes, container, false);

        activity = getActivity();
        out = DefinedDialog.CreateProgressDialog(activity, null);

        sp = new SecurePreferences(activity);

        layout_max_partial = (LinearLayout) view.findViewById(R.id.layout_max_partial);
        layout_input_amount = (LinearLayout) view.findViewById(R.id.layout_input_amount);
        layout_token = (LinearLayout) view.findViewById(R.id.layout_token);
        et_input_amount = (EditText) view.findViewById(R.id.input_amount_value);
        et_token = (EditText) view.findViewById(R.id.token_value);
        tv_tx_id = (TextView) view.findViewById(R.id.tx_id_value);
        tv_comm_name = (TextView) view.findViewById(R.id.comm_name_value);
        tv_bank_name = (TextView) view.findViewById(R.id.bank_name_value);
        tv_bank_product = (TextView) view.findViewById(R.id.bank_product_value);
        tv_ccy_id = (TextView) view.findViewById(R.id.ccy_id_value);
        tv_amount = (TextView) view.findViewById(R.id.amount_value);
        tv_admin_fee = (TextView) view.findViewById(R.id.admin_fee_value);
        tv_desc = (TextView) view.findViewById(R.id.desc_value);
        tv_max_partial = (TextView) view.findViewById(R.id.max_partial_value);
        btnProcess = (Button) view.findViewById(R.id.preview_btn_process);
        btnCancel = (Button) view.findViewById(R.id.preview_btn_cancel);

        Bundle bundle = getArguments();
        if(bundle != null) {
            tx_id = bundle.getString(AppParams.TX_ID);
            bank_code = bundle.getString(AppParams.BANK_CODE);
            bank_name = bundle.getString(AppParams.BANK_NAME);
            product_code = bundle.getString(AppParams.PRODUCT_CODE);
            product_name = bundle.getString(AppParams.PRODUCT_NAME);

            comm_id = bundle.getString(AppParams.COMM_ID);
            comm_name = bundle.getString(AppParams.COMM_NAME);
            comm_code = bundle.getString(AppParams.COMM_CODE);

            inquiryTrxKMK();
        }

        btnProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnProcess.setEnabled(false);
                btnProcess.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        btnProcess.setEnabled(true);
                    }
                }, 3000);

                if (layout_input_amount.getVisibility() == View.VISIBLE && inputAmountValidation() && layout_token.getVisibility() == View.GONE) {
                    requestTokenKMK(et_input_amount.getText().toString());
                }
                else if(layout_token.getVisibility() == View.VISIBLE){
                    if (inputTokenValidation()) {
                        confirmTokenKMK(et_token.getText().toString());
                    }
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                backToPayment();
            }
        });

        return view;
    }

    public void backToPayment(){
        hideKeyboard();
        Fragment newFragment = new FragmentPaymentRepayment();
        Bundle args = new Bundle();
        args.putString(AppParams.COMM_ID, comm_id);
        args.putString(AppParams.COMM_CODE, comm_code);
        args.putString(AppParams.COMM_NAME, comm_name);
        newFragment.setArguments(args);
        switchFragment(newFragment);
    }

    public void backToCommunity(){
        hideKeyboard();
        Fragment newFragment = new FragmentCommunityRepayment();
        Bundle bundle = new Bundle();
        bundle.putString("title", getResources().getString(R.string.kmk));
        newFragment.setArguments(bundle);
        switchFragment(newFragment);
    }

    public void confirmTokenKMK(String _token) {
        try {
            out.show();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());

            RequestParams params = new RequestParams();
            params.put(AppParams.TX_ID, tx_id);
            params.put(AppParams.COMM_CODE, comm_code);
            params.put(AppParams.TOKEN_ID, _token);

            Log.d("confirm token params", params.toString());
            client.post(AplConstants.ConfirmTokenKMKAPI, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("confirm token response:", content);
                    try {
                        out.dismiss();
                        JSONObject json = new JSONObject(content);

                        String error_code = json.getString("error_code");
                        String error_msg = json.getString("error_message");

                        if (error_code.equals(AppParams.SUCCESS_CODE)) {
                            backToCommunity();
                        }
                        else if(error_code.equals("0031")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                            builder.setTitle("Alert");
                            builder.setMessage(error_msg);
                            builder.setCancelable(false);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    backToPayment();
                                }
                            });
                            alert = builder.create();
                            alert.show();
                        }
                        else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                            builder.setTitle("Alert");
                            builder.setMessage(error_msg);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            alert = builder.create();
                            alert.show();
                        }
                    }
                    catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                };

                public void onFailure(Throwable error, String content) {
                    Toast.makeText(activity, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            });
        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }

    public void requestTokenKMK(String _amount){
        try {
            out.show();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
            client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);

            RequestParams params = new RequestParams();
            params.put("tx_id", tx_id);
            params.put("partial_amount", _amount);
            params.put("comm_code", comm_code);

            Log.d("params", params.toString());
            client.post(AplConstants.ReqTokenKMKAPI, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("result:", content);
                    try {
                        // parse json data and store into arraylist variables
                        JSONObject json = new JSONObject(content);
                        String error_code = json.getString("error_code");
                        String error_message = json.getString("error_message");

                        out.dismiss();

                        if (error_code.equals(AppParams.SUCCESS_CODE)) {
                            et_input_amount.setEnabled(false);
                            layout_token.setVisibility(View.VISIBLE);

                        }
                        else if (error_code.equals("0031")){
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setMessage(error_message)
                                    .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                        else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setMessage(error_message)
                                    .setCancelable(false)
                                    .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            backToPayment();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();

                    }
                }

                ;

                public void onFailure(Throwable error, String content) {
                    Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }

                ;
            });
        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }

    public void inquiryTrxKMK(){
        try {
            out.show();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
            client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);

            RequestParams params = new RequestParams();
            params.put("tx_id", tx_id);
            params.put("product_code", product_code);
            params.put("comm_code", comm_code);

            Log.d("params", params.toString());
            client.post(AplConstants.InquiryTrxKMKAPI, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("result:", content);
                    try {
                        // parse json data and store into arraylist variables
                        JSONObject json = new JSONObject(content);
                        String error_code = json.getString("error_code");
                        String error_message = json.getString("error_message");

                        out.dismiss();

                        if (error_code.equals(AppParams.SUCCESS_CODE)) {
                            String amount = json.getString(AppParams.AMOUNT);

                            tv_tx_id.setText(json.getString(AppParams.TX_ID));
                            tv_comm_name.setText(comm_name);
                            tv_admin_fee.setText(json.getString(AppParams.ADMIN_FEE));
                            tv_amount.setText(amount);
                            tv_bank_name.setText(bank_name);
                            tv_bank_product.setText(product_name);
                            tv_ccy_id.setText(json.getString(AppParams.CCY_ID));
                            tv_desc.setText(json.getString(AppParams.DESCRIPTION));

                            flag_partial = json.getString(AppParams.FLAG_PARTIAL);
                            String max_partial = json.getString(AppParams.MAX_PARTIAL);

                            if(flag_partial.equalsIgnoreCase("Y")) {
                                layout_max_partial.setVisibility(View.VISIBLE);
                                layout_input_amount.setVisibility(View.VISIBLE);
                                tv_max_partial.setText(max_partial);
                            }
                            else {
                                requestTokenKMK("0");
                            }

                        }
                        else if(error_code.equals("0003") || error_code.equals("0059") || error_code.equals("0031")){
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setMessage(error_message)
                                    .setCancelable(false)
                                    .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            backToPayment();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                        else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setMessage(error_message)
                                    .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();

                    }
                }

                ;

                public void onFailure(Throwable error, String content) {
                    Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }

                ;
            });
        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }


    public boolean inputAmountValidation(){
        if(et_input_amount.getText().toString().length()==0 || Long.parseLong(et_input_amount.getText().toString()) <= 0){
            et_input_amount.requestFocus();
            et_input_amount.setError(this.getString(R.string.validation_amount));
            return false;
        }
        return true;
    }

    public boolean inputTokenValidation(){
        if(et_token.getText().toString().length()==0){
            et_token.requestFocus();
            et_token.setError(this.getString(R.string.validation_otp));
            return false;
        }
        return true;
    }


    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

    public void hideKeyboard(){
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
