package sgo.mobile.mava.app.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.slidingmenu.lib.SlidingMenu;
import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.ui.slidemenu.SlideoutMenuFragment;
import sgo.mobile.mava.app.ui.slidemenu.SlidingSherlockFragmentActivity;

public abstract class BaseMainActivity extends SlidingSherlockFragmentActivity implements ActionBar.OnNavigationListener {

	private static final String TAG = "BaseMainActivity";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addSlideMenu();
        actionBarSetup();
    }


    @SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	protected void addSlideMenu() {
        setBehindContentView(R.layout.fragment_slideoutmenu);

        FragmentTransaction t = this.getSupportFragmentManager().beginTransaction();
        Fragment fragment = new SlideoutMenuFragment();
        t.replace(R.id.fragment_slideoutmenu, fragment);
        t.commit();
        
        Point screenSize = AppHelper.screenSize(getApplicationContext());
        SlidingMenu mSlidingMenu = getSlidingMenu();
        mSlidingMenu.setShadowWidthRes(R.dimen.shadow_width);
        mSlidingMenu.setShadowDrawable(R.drawable.shadow);
        mSlidingMenu.setSelectorEnabled(true);
        mSlidingMenu.setSelectorDrawable(R.drawable.shadow_second);

        mSlidingMenu.setBehindWidth((int) (screenSize.x*0.85));

        setSlidingActionBarEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        String user_id = AppHelper.getUserId(this);
        menu.findItem(R.id.action_user).setTitle(user_id);
        return true;
    }

    protected void actionBarSetup()
    {
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayShowTitleEnabled(true);
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setTitle(R.string.ab_title);
//        actionBar.setSubtitle(R.string.ab_subtitle);
//        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
//        	actionBar.setHomeButtonEnabled(true);
//        }

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            actionBar.setHomeButtonEnabled(true);

        }

        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.toolbar_main, null);

        actionBar.setCustomView(v);
        actionBar.setDisplayShowCustomEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Log.d(TAG, "onOptionsItemSelected: " + item.getItemId());
        switch(item.getItemId()) {
            case android.R.id.home:
                getSlidingMenu().toggle();
                return true;
            case R.id.action_profile:
                Intent intent = new Intent(this, ActivityChangePassword.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		return true;
	}

    public int getDpAsPxFromResource(int res) {
        return (int)getResources().getDimension(res);
    }
}
