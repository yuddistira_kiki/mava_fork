package sgo.mobile.mava.app.beans;

/**
 * Created by thinkpad on 1/21/2016.
 */
public class SpinnerBankBean {
    private String bank_code;
    private String bank_name;

    public SpinnerBankBean(String bank_code, String bank_name) {
        this.bank_code = bank_code;
        this.bank_name = bank_name;
    }

    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }
}
