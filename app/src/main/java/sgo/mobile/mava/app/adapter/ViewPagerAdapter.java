package sgo.mobile.mava.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import sgo.mobile.mava.app.fragments.FragmentC2CCashIn;
import sgo.mobile.mava.app.fragments.FragmentC2CCashOut;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private final int PAGES = 2;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentC2CCashIn();
            case 1:
                return new FragmentC2CCashOut();
            default:
                throw new IllegalArgumentException("The item position should be less or equal to:" + PAGES);
        }
    }

    @Override
    public int getCount() {
        return PAGES;
    }
}
