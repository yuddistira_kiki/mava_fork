package sgo.mobile.mava.app.beans;

public class InvoiceBean {
    public String doc_no = "";
    public String doc_id = "";
    public String amount = "";
    public String remain_amount = "";
    public String ccy     = "";
    public String partial = "";
    public String community = "";
    public String member    = "";

    public InvoiceBean( String _doc_no, String _doc_id, String _remain_amount, String _ccy, String _partial, String _community,String _member)
    {
        doc_no         = _doc_no;
        doc_id         = _doc_id;
        remain_amount  = _remain_amount;
        ccy            = _ccy;
        partial        = _partial;
        community      = _community;
        member         = _member;
    }

    public String getDoc_no(){
        return doc_no;
    }

    public void setDoc_no(String doc_no){
        this.doc_no = doc_no;
    }

    public String getDoc_id(){
        return doc_id;
    }

    public void setDoc_id(String doc_id){
        this.doc_id = doc_id;
    }

    public String getAmount(){
        return amount;
    }

    public void setAmount(String amount){
        this.amount = amount;
    }

    public String getRemain_amount()
    {
        return remain_amount;
    }

    public void setRemain_amount(String remain_amount){
        this.remain_amount = remain_amount;
    }

    public String getCcy(){
        return ccy;
    }

    public void setCcy(String ccy){
        this.ccy = ccy;
    }

    public String getPartial(){
        return partial;
    }

    public void setPartial(String partial){
        this.partial = partial;
    }

    public String getCommunity(){
        return community;
    }

    public void setCommunity(String community){
        this.community = community;
    }

    public String getMember(){
        return member;
    }

    public void setMember(String member){
        this.member = member;
    }

    public String toString()
    {
        return(  doc_no + " " + doc_id );
    }
}