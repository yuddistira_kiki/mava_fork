package sgo.mobile.mava.app.beans;

/**
 * Created by thinkpad on 1/21/2016.
 */
public class SpinnerProductBean {
    private String product_code;
    private String product_name;

    public SpinnerProductBean(String product_code, String product_name){
        this.product_code = product_code;
        this.product_name = product_name;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }
}
