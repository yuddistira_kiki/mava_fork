package sgo.mobile.mava.app.beans;

public class BankCCLBean {
    public String bank_code = "";
    public String bank_name = "";
    public String ccy_id = "";
    public String product_code = "";
    public String member_phone = "";
    public String benef_acct_no = "";
    public String benef_acct_name = "";
    public String tx_fee = "";
    public String fee_acct_no = "";
    public String seller_fee = "";
    public String charges_acct_no = "";
    public String buyer_fee = "";
    public String commission_acct_no = "";
    public String commission_fee = "";
    public String bbs_product_json = "";

    public BankCCLBean(String bank_code, String bank_name, String ccy_id, String product_code, String member_phone, String benef_acct_no, String benef_acct_name, String tx_fee, String fee_acct_no, String seller_fee, String charges_acct_no, String buyer_fee, String commission_acct_no, String commission_fee, String bbs_product_json) {
        this.bank_code = bank_code;
        this.bank_name = bank_name;
        this.ccy_id = ccy_id;
        this.product_code = product_code;
        this.member_phone = member_phone;
        this.benef_acct_no = benef_acct_no;
        this.benef_acct_name = benef_acct_name;
        this.tx_fee = tx_fee;
        this.fee_acct_no = fee_acct_no;
        this.seller_fee = seller_fee;
        this.charges_acct_no = charges_acct_no;
        this.buyer_fee = buyer_fee;
        this.commission_acct_no = commission_acct_no;
        this.commission_fee = commission_fee;
        this.bbs_product_json = bbs_product_json;
    }

    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getCcy_id() {
        return ccy_id;
    }

    public void setCcy_id(String ccy_id) {
        this.ccy_id = ccy_id;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getMember_phone() {
        return member_phone;
    }

    public void setMember_phone(String member_phone) {
        this.member_phone = member_phone;
    }

    public String getBenef_acct_no() {
        return benef_acct_no;
    }

    public void setBenef_acct_no(String benef_acct_no) {
        this.benef_acct_no = benef_acct_no;
    }

    public String getBenef_acct_name() {
        return benef_acct_name;
    }

    public void setBenef_acct_name(String benef_acct_name) {
        this.benef_acct_name = benef_acct_name;
    }

    public String getTx_fee() {
        return tx_fee;
    }

    public void setTx_fee(String tx_fee) {
        this.tx_fee = tx_fee;
    }

    public String getFee_acct_no() {
        return fee_acct_no;
    }

    public void setFee_acct_no(String fee_acct_no) {
        this.fee_acct_no = fee_acct_no;
    }

    public String getSeller_fee() {
        return seller_fee;
    }

    public void setSeller_fee(String seller_fee) {
        this.seller_fee = seller_fee;
    }

    public String getCharges_acct_no() {
        return charges_acct_no;
    }

    public void setCharges_acct_no(String charges_acct_no) {
        this.charges_acct_no = charges_acct_no;
    }

    public String getBuyer_fee() {
        return buyer_fee;
    }

    public void setBuyer_fee(String buyer_fee) {
        this.buyer_fee = buyer_fee;
    }

    public String getCommission_acct_no() {
        return commission_acct_no;
    }

    public void setCommission_acct_no(String commission_acct_no) {
        this.commission_acct_no = commission_acct_no;
    }

    public String getCommission_fee() {
        return commission_fee;
    }

    public void setCommission_fee(String commission_fee) {
        this.commission_fee = commission_fee;
    }

    public String getBbs_product_json() {
        return bbs_product_json;
    }

    public void setBbs_product_json(String bbs_product_json) {
        this.bbs_product_json = bbs_product_json;
    }

    @Override
    public String toString() {
        return bank_name;
    }
}
