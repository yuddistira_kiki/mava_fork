package sgo.mobile.mava.app.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.adapter.MemberCCLDIAdapter;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

import java.util.ArrayList;

public class FragmentMemberCCLDI extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;

    public String comm_id, comm_name, comm_code, buss_scheme_code;

    // declare view objects
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert;
    TextView lbl_header;

    MemberCCLDIAdapter MemberCCLDIAdapter;

    // create arraylist variables to store data from server
    public static ArrayList<String> id   = new ArrayList<String>();
    public static ArrayList<String> code = new ArrayList<String>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_member_list, container, false);
        txtAlert = (TextView) view.findViewById(R.id.txtAlert);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        lbl_header = (TextView) view.findViewById(R.id.label_header);

        Bundle bundle         = this.getArguments();
        comm_id               = bundle.getString("comm_id");
        comm_name             = bundle.getString("comm_name");
        comm_code             = bundle.getString("comm_code");
        buss_scheme_code      = bundle.getString("buss_scheme_code");

        MemberCCLDIAdapter = new MemberCCLDIAdapter(getActivity());
        parseJSONData();

        lbl_header = (TextView) view.findViewById(R.id.label_header);
        lbl_header.setText(comm_name);

        // event listener to handle list when clicked
        listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {

                Fragment newFragment = null;
                newFragment = new FragmentInputSalesAlias();
                Bundle args = new Bundle();
                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("buss_scheme_code",buss_scheme_code);
                args.putString("member_id", id.get(position));
                args.putString("member_code", code.get(position));
                newFragment.setArguments(args);
                switchFragment(newFragment);

            }
        });


        return view;
    }

    public void parseJSONData(){
        clearData();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();


        params.put("comm_id", comm_id);
        String param_cust_id = AppHelper.getCustomerId(getActivity());
        params.put("cust_id", param_cust_id);

        Log.d("params", params.toString());
        client.post(AplConstants.MemberCCLDIMobileAPI, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {
                Log.d("result:", content);
                try {
                    // parse json data and store into arraylist variables
                    JSONObject json = new JSONObject(content);
                    String error_code         = json.getString("error_code");
                    String error_message      = json.getString("error_message");

                    if (error_code.equals(AppParams.SUCCESS_CODE)) {
                        JSONArray data = json.getJSONArray("member_data"); // this is the "items: [ ] part
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject object = data.getJSONObject(i);
                            id.add(object.getString("member_id"));
                            code.add(object.getString("member_code"));
                        }

                        // when finish parsing, hide progressbar
                        prgLoading.setVisibility(8);

                        // if data available show data on list
                        // otherwise, show alert text
                        if(id.size() > 0){
                            listMenu.setVisibility(0);
                            listMenu.setAdapter(MemberCCLDIAdapter);
                            lbl_header.setVisibility(0);
                        }else{
                            txtAlert.setVisibility(0);
                        }

                    }else{
                        prgLoading.setVisibility(8);
                        txtAlert.setVisibility(0);
                        txtAlert.setText(error_message);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            }

            ;

            public void onFailure(Throwable error, String content) {
                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }

            ;
        });
    }

    // clear arraylist variables before used
    void clearData(){
        id.clear();
        code.clear();
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

}