package sgo.mobile.mava.app.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.google.gson.Gson;
import com.securepreferences.SecurePreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.activities.SgoPlusWeb;
import sgo.mobile.mava.app.adapter.InvoiceRepaymentAdapter;
import sgo.mobile.mava.app.adapter.PaymentTypeAdapter;
import sgo.mobile.mava.app.beans.InvoiceRepaymentBean;
import sgo.mobile.mava.app.beans.PaymentTypeBean;
import sgo.mobile.mava.app.beans.SelectAgainSpinner;
import sgo.mobile.mava.app.dialogs.DefinedDialog;
import sgo.mobile.mava.app.dialogs.DialogChoosePaymentKMK;
import sgo.mobile.mava.app.dialogs.DialogRepayment;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.conf.ReqPermissionClass;
import sgo.mobile.mava.conf.SMSclass;
import sgo.mobile.mava.frameworks.math.FormatCurrency;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

/**
 * Created by thinkpad on 1/19/2016.
 */
public class FragmentPaymentRepayment extends Fragment implements DialogRepayment.NoticeDialogListener, DialogChoosePaymentKMK.NoticeDialogListener{

    ProgressDialog out;
    Activity activity;
    SecurePreferences sp;
    FragmentManager fm;

    private ArrayList<PaymentTypeBean> PaymentTypeList = new ArrayList<PaymentTypeBean>();
    private static SelectAgainSpinner cbo_payment_type      = null;
    public String payment_type_code = "",
            payment_type_name = null,
            payment_bankname = null,
            payment_bankcode = null,
            payment_productcode = null,
            payment_productname = null,
            payment_product_h2h = null,
    tx_id, comm_id_2, comm_name_2, comm_code_2, ccy_id, amount, admin_fee, api_key;

    DialogRepayment dialog;
    boolean alertShow = false;

    public String comm_id, comm_name, comm_code;

    // declare view objects
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert;
    TextView lbl_header;
    TableLayout tabel_footer;
    TextView lbl_total_pay_amount;


    InvoiceRepaymentAdapter adapter;
    List<InvoiceRepaymentBean> listInvoice;

    Button btnProcess;
    Button btnCancel;

    // create price format
    DecimalFormat formatData = new DecimalFormat("#.##");

    List<String> invoiceSelected;

    long totalBayar = 0;

    int start = 0;
    public static boolean changeFragment = false;
    AlertDialog alert = null;
    private ReqPermissionClass reqPermissionClass;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_paymenttype_repayment, container, false);

        activity = getActivity();
        sp = new SecurePreferences(activity);
        listInvoice = new ArrayList<>();
        invoiceSelected = new ArrayList<>();

        txtAlert = (TextView) view.findViewById(R.id.txtAlert);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        tabel_footer = (TableLayout) view.findViewById(R.id.tabel_footer);
        lbl_total_pay_amount = (TextView) view.findViewById(R.id.lbl_total_pay_amount);

        btnProcess               = (Button) view.findViewById(R.id.btnDone);
        btnCancel             = (Button) view.findViewById(R.id.btnCancel);

        cbo_payment_type      = (SelectAgainSpinner)view.findViewById(R.id.cbo_payment_type);

        Bundle bundle         = this.getArguments();
        comm_id               = bundle.getString(AppParams.COMM_ID);
        comm_name             = bundle.getString(AppParams.COMM_NAME);
        comm_code             = bundle.getString(AppParams.COMM_CODE);


        lbl_header = (TextView) view.findViewById(R.id.label_header);
        lbl_header.setText(getResources().getString(R.string.invoice_kmk));

        adapter = new InvoiceRepaymentAdapter(getActivity(), listInvoice, false);
        getInvoiceFromDB();

        // hardcoded payment type
        PaymentTypeList.add(new PaymentTypeBean("0", getResources().getString(R.string.pilih_tipe_pembayaran)));
        PaymentTypeList.add(new PaymentTypeBean("ESPAY", "Espay"));

        PaymentTypeAdapter paymentTypeAdapter = new PaymentTypeAdapter(activity, android.R.layout.simple_spinner_item, PaymentTypeList) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;

                // If this is the initial dummy entry, make it hidden
                if (position == 0) {
                    TextView tv = new TextView(getContext());
                    tv.setHeight(0);
                    tv.setVisibility(View.GONE);
                    v = tv;
                } else {
                    // Pass convertView as null to prevent reuse of special case views
                    v = super.getDropDownView(position, null, parent);
                }

                // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
                parent.setVerticalScrollBarEnabled(false);
                return v;
            }
        };

        cbo_payment_type.setAdapter(paymentTypeAdapter);
        cbo_payment_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {

                } else {
                    if (alertShow) return;
                    String type = null;

                    type = PaymentTypeList.get(position).getPayment_type();

                    if (PaymentTypeList.get(position).getPayment_type().equalsIgnoreCase(payment_type_code)) {
                        alertShow = true;
                        FragmentManager fragmentManager = getFragmentManager();
                        dialog = new DialogRepayment();
                        Bundle args = new Bundle();
                        args.putString("payment_type", type);
                        args.putString("payment_bank_code", payment_bankcode);
                        args.putString("payment_product_code", payment_productcode);
                        dialog.setArguments(args);
                        dialog.setCancelable(false);
                        dialog.setTargetFragment(FragmentPaymentRepayment.this, 100);
                        dialog.show(fragmentManager, "dialogRepayment");
                    } else {
                        alertShow = true;
                        FragmentManager fragmentManager = getFragmentManager();
                        dialog = new DialogRepayment();
                        Bundle args = new Bundle();
                        args.putString("payment_type", type);
                        args.putString("payment_bank_code", "0");
                        args.putString("payment_product_code", "0");
                        dialog.setArguments(args);
                        dialog.setCancelable(false);
                        dialog.setTargetFragment(FragmentPaymentRepayment.this, 100);
                        dialog.show(fragmentManager, "dialogrepayment");
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnProcess.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                btnProcess.setEnabled(false);
                btnProcess.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        btnProcess.setEnabled(true);
                    }
                }, 3000);

                if (payment_type_code.equalsIgnoreCase(AppParams.ESPAY)) {
                    if (totalBayar > 0 && !payment_type_code.equals("") && payment_productcode != null && payment_bankcode != null) {
                        PaymentTypeBean paymentSelected = (PaymentTypeBean) cbo_payment_type.getSelectedItem();
                        payment_type_code = String.valueOf(paymentSelected.payment_type);
                        payment_type_name = String.valueOf(paymentSelected.payment_name);

                        repayInvKMK(payment_productcode, payment_bankcode);

                    } else if (payment_type_code.equals("")) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.alert_item_belum_diisi), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.alert_item_belum_diisi), Toast.LENGTH_SHORT).show();
                    }
                } else if (payment_type_code.equals("")) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.alert_item_belum_diisi), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.alert_item_belum_diisi), Toast.LENGTH_SHORT).show();
                }
            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = null;
                newFragment = new FragmentInvRepaymentList();

                Bundle args = new Bundle();
                args.putString(AppParams.COMM_ID, comm_id);
                args.putString(AppParams.COMM_NAME, comm_name);
                args.putString(AppParams.COMM_CODE, comm_code);

                newFragment.setArguments(args);
                getActivity().getSupportFragmentManager().popBackStack();
                switchFragment(newFragment);
            }
        });

        return view;
    }

    public void repayInvKMK(String _product_code, String _bank_code){
        try {
            out = DefinedDialog.CreateProgressDialog(activity, null);

            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
            client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);

            Gson gson = new Gson();
            String strInvoiceSelected = gson.toJson(invoiceSelected);

            RequestParams params = new RequestParams();
            params.put("tx_id", strInvoiceSelected);
            params.put("product_code", _product_code);
            params.put("bank_code", _bank_code);

            Log.d("params", params.toString());
            client.post(AplConstants.RepayInvKMKAPI, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("result:", content);
                    try {
                        // parse json data and store into arraylist variables
                        JSONObject json = new JSONObject(content);
                        String error_code = json.getString("error_code");
                        String error_message = json.getString("error_message");

                        out.dismiss();

                        if (error_code.equals(AppParams.SUCCESS_CODE)) {
                            tx_id = json.getString(AppParams.TX_ID);
                            String tx_kmk = json.getString(AppParams.TX_KMK);
                            ccy_id = json.getString(AppParams.CCY_ID);
                            amount = json.getString(AppParams.AMOUNT);
                            admin_fee = json.getString(AppParams.ADMIN_FEE);
                            comm_id_2 = json.getString(AppParams.COMM_ID);
                            comm_name_2 = json.getString(AppParams.COMM_NAME);
                            comm_code_2 = json.getString(AppParams.COMM_CODE);
                            api_key = json.getString(AppParams.API_KEY);

                            if(tx_kmk.equalsIgnoreCase("Y")) {
                                setIfTxKmkY();
                            }
                            else if(tx_kmk.equalsIgnoreCase("N")) {
                                setIfTxKmkN();
                            }
                            else if(tx_kmk.equalsIgnoreCase("B")) {
                                FragmentManager fragmentManager = getFragmentManager();
                                DialogChoosePaymentKMK dialog = new DialogChoosePaymentKMK();
                                dialog.setCancelable(false);
                                dialog.setTargetFragment(FragmentPaymentRepayment.this, 100);
                                dialog.show(fragmentManager, "dialogChoosePayment");
                            }
                            else {
                                Toast.makeText(activity, getResources().getString(R.string.tx_kmk_tidak_sesuai), Toast.LENGTH_LONG).show();
                            }
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setMessage(error_message)
                                    .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alert = builder.create();
                            alert.show();
                        }


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();

                    }
                }

                ;

                public void onFailure(Throwable error, String content) {
                    Toast.makeText(activity, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }

                ;
            });
        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }

    public void getbankKMK(){
        try {
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
            client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);

            Gson gson = new Gson();
            String strInvoiceSelected = gson.toJson(invoiceSelected);

            RequestParams params = new RequestParams();
            params.put("tx_id", strInvoiceSelected);
            params.put("comm_id", comm_id);

            Log.d("params", params.toString());
            client.post(AplConstants.BankKMKAPI, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("result:", content);
                    try {
                        // parse json data and store into arraylist variables
                        JSONObject json = new JSONObject(content);
                        String error_code         = json.getString("error_code");
                        String error_message      = json.getString("error_message");

                        prgLoading.setVisibility(View.GONE);

                        if (error_code.equals(AppParams.SUCCESS_CODE)) {

                            String bankKMK = json.getString(AppParams.BANK_DATA);

                            SecurePreferences.Editor mEditor = sp.edit();
                            mEditor.putString(AppParams.BANK_KMK, bankKMK);
                            mEditor.apply();

                            if (listInvoice.size() > 0) {
                                listMenu.setVisibility(View.VISIBLE);
                                listMenu.setAdapter(adapter);
                                lbl_header.setVisibility(View.VISIBLE);
                                tabel_footer.setVisibility(View.VISIBLE);
                            } else {
                                txtAlert.setVisibility(View.VISIBLE);
                            }

                        }else{
                            txtAlert.setVisibility(View.VISIBLE);
                            txtAlert.setText(error_message);
                        }


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();

                    }
                };

                public void onFailure(Throwable error, String content) {
                    Toast.makeText(activity, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }

                ;
            });
        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }

    public void getInvoiceFromDB() {
        ActiveAndroid.beginTransaction();

        List<InvoiceRepaymentBean> listInvoiceAll = InvoiceRepaymentBean.getAll();

        Log.d("arrayPost length", String.valueOf(listInvoiceAll.size()));
        if (listInvoiceAll.size() > 0) {
            for (int i = 0; i < listInvoiceAll.size(); i++) {
                if(listInvoiceAll.get(i).getSelected().equals("1")) {
                    listInvoice.add(new InvoiceRepaymentBean(listInvoiceAll.get(i).getTx_id(),
                            listInvoiceAll.get(i).getMax_tx_date(),
                            listInvoiceAll.get(i).getAmount(),
                            listInvoiceAll.get(i).getCcy_id(),
                            listInvoiceAll.get(i).getDesc(),
                            listInvoiceAll.get(i).getSelected()));
                    invoiceSelected.add(listInvoiceAll.get(i).getTx_id());
                    totalBayar += Long.parseLong(listInvoiceAll.get(i).getAmount());
                }
            }
            lbl_total_pay_amount.setText(FormatCurrency.getRupiahFormat(Long.toString(totalBayar)));
        }

        ActiveAndroid.setTransactionSuccessful();
        ActiveAndroid.endTransaction();

        getbankKMK();
    }

    public void setIfTxKmkY(){

        if(payment_product_h2h.equalsIgnoreCase("Y")) {
            Fragment newFragment = null;
            newFragment = new FragmentPreviewRepayYes();
            Bundle args = new Bundle();
            args.putString(AppParams.TX_ID, tx_id);
            args.putString(AppParams.BANK_CODE, payment_bankcode);
            args.putString(AppParams.BANK_NAME, payment_bankname);
            args.putString(AppParams.PRODUCT_CODE, payment_productcode);
            args.putString(AppParams.PRODUCT_NAME, payment_productname);

            args.putString(AppParams.COMM_ID, comm_id_2);
            args.putString(AppParams.COMM_NAME, comm_name_2);
            args.putString(AppParams.COMM_CODE, comm_code_2);

            newFragment.setArguments(args);
            switchFragment(newFragment);
        }
        else if(payment_product_h2h.equalsIgnoreCase("N")){

        }
    }

    public void setIfTxKmkN(){
        if(payment_product_h2h.equalsIgnoreCase("Y")) {
            Fragment newFragment = new FragmentPreviewRepayNo();
            Bundle args = new Bundle();
            args.putString(AppParams.TX_ID, tx_id);
            args.putString(AppParams.BANK_CODE, payment_bankcode);
            args.putString(AppParams.BANK_NAME, payment_bankname);
            args.putString(AppParams.PRODUCT_CODE, payment_productcode);
            args.putString(AppParams.PRODUCT_NAME, payment_productname);

            args.putString(AppParams.COMM_ID, comm_id_2);
            args.putString(AppParams.COMM_NAME, comm_name_2);
            args.putString(AppParams.COMM_CODE, comm_code_2);
            args.putString(AppParams.CCY_ID, ccy_id);
            args.putString(AppParams.AMOUNT, amount);
            args.putString(AppParams.ADMIN_FEE, admin_fee);

            newFragment.setArguments(args);
            switchFragment(newFragment);
        }
        else if(payment_product_h2h.equalsIgnoreCase("N")){

            long total = Long.parseLong(amount) + Long.parseLong(admin_fee);
            Intent i = new Intent(activity, SgoPlusWeb.class);
            i.putExtra(AppParams.REPAYMENT, "Y");
            i.putExtra(AppParams.PRODUCT_CODE, payment_productcode);
            i.putExtra(AppParams.PRODUCT_NAME, payment_productname);
            i.putExtra(AppParams.BANK_CODE, payment_bankcode);
            i.putExtra(AppParams.BANK_NAME, payment_bankname);
            i.putExtra(AppParams.COMM_CODE, comm_code_2);
            i.putExtra(AppParams.TX_ID, tx_id);
            i.putExtra(AppParams.COMM_ID, comm_id_2);
            i.putExtra(AppParams.API_KEY, api_key);
            i.putExtra(AppParams.TX_AMOUNT, amount);
            i.putExtra(AppParams.ADMIN_FEE, admin_fee);
            i.putExtra(AppParams.TOTAL_PAY, Long.toString(total));
            i.putExtra(AppParams.TX_MULTIPLE, "N");

            switchActivity(i);
        }
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

    @Override
    public void onFinishDialog() {
        PaymentTypeBean paymentSelected  = (PaymentTypeBean) cbo_payment_type.getSelectedItem();
        payment_type_code   = String.valueOf(paymentSelected.payment_type);
        payment_type_name   = String.valueOf(paymentSelected.payment_name);
        alertShow = false;
    }

    @Override
    public void onFinishSpinnerBankDialog(String bankCode, String bankName) {
        payment_bankcode = bankCode;
        payment_bankname = bankName;
        alertShow = false;
    }

    @Override
    public void onFinishSpinnerProductDialog(String productCode, String productName, String product_h2h) {
        payment_productcode = productCode;
        payment_productname = productName;
        payment_product_h2h = product_h2h;
        alertShow = false;
    }

    @Override
    public void onDismiss(boolean dismiss) {
        if(dismiss == true) {
            cbo_payment_type.setSelection(0);
            payment_bankcode = "";
            payment_bankname = "";
            alertShow = false;
        }
    }


    private void switchActivity(Intent mIntent){
        if (getActivity() == null)
            return;

        MainActivity fca = (MainActivity) getActivity();
        fca.switchActivity(mIntent, MainActivity.ACTIVITY_RESULT);
    }


    @Override
    public void onStart() {
        super.onStart();

        if(start == 0) {
            start++;
            changeFragment = false;
        }
        if(changeFragment == false) {

        }
        else {

            if(alert != null){
                if (alert.isShowing()) {
                    alert.dismiss();
                }
            }
            Fragment newFragment = new FragmentCommunityRepayment();
            Bundle args = new Bundle();
            args.putString("title", getResources().getString(R.string.kmk));
            args.putString(AppParams.COMM_ID, comm_id);
            args.putString(AppParams.COMM_NAME, comm_name);
            args.putString(AppParams.COMM_CODE, comm_code);

            newFragment.setArguments(args);
            switchFragment(newFragment);
        }
    }

    @Override
    public void onChoosePaymentKMK(String rbSelected) {
        if(rbSelected.equalsIgnoreCase(getResources().getString(R.string.kmk))) {
            setIfTxKmkY();
        }
        else if(rbSelected.equalsIgnoreCase(getResources().getString(R.string.saving_giro))) {
            setIfTxKmkN();
        }
    }

    private void initializeSMSBanking(){
        reqPermissionClass = new ReqPermissionClass(getActivity());
        if(reqPermissionClass.checkPermission(Manifest.permission.READ_PHONE_STATE,ReqPermissionClass.PERMISSIONS_REQ_READPHONESTATE)){
            initializeSmsClass();
        }
    }

    private void initializeSmsClass(){
        SMSclass smSclass = new SMSclass(getActivity());

        smSclass.isSimExists(new SMSclass.SMS_SIM_STATE() {
            @Override
            public void sim_state(Boolean isExist, String msg) {
                if(!isExist){
                    Toast.makeText(getActivity(),msg,Toast.LENGTH_LONG).show();
                    getActivity().finish();
                }
            }
        });

        try{
            getActivity().unregisterReceiver(smSclass.simStateReceiver);
        }
        catch (Exception ignored){}
        getActivity().registerReceiver(smSclass.simStateReceiver,SMSclass.simStateIntentFilter);
    }
}
