package sgo.mobile.mava.app.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.*;
import org.json.JSONException;
import org.json.JSONObject;

import sgo.mobile.mava.BuildConfig;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.ui.dialog.DefinedDialog;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;
import sgo.mobile.mava.frameworks.security.Md5;

import java.security.NoSuchAlgorithmException;

public class ActivityLogin extends BaseActivity {

    private RadioGroup OptGroupUserType;
    private RadioButton radioLogin;

    private Button btnLogin;
    private EditText txtPin;
    private EditText txtUserID;
    private String phone, password;
    private boolean isExit;
    private ProgressDialog pDialog;
    private SharedPreferences sp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getAppVersion();
        findViews();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void exit(){
        if (!isExit) {
            isExit = true;
            Toast.makeText(getApplicationContext(), "Exit", Toast.LENGTH_SHORT).show();
            mHandler.sendEmptyMessageDelayed(0, 2000);
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
            System.exit(0);
        }
    }

    public void ForgotPin(View view)
    {
        Intent intent = new Intent(ActivityLogin.this, ActivityResetPassword.class);
        startActivity(intent);
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            isExit = false;
        }

    };

    public String editNoHP (String noHP){
        String result = null;
        if(noHP.charAt(0) == '0')result = "62"+noHP.substring(1);
        else if(noHP.charAt(0) == '+' )result = noHP.substring(1);
        else if(noHP.charAt(0) == '6' && noHP.charAt(1) == '2') result = noHP;
        else result = "62"+noHP;

        return result.replaceAll("[\\s\\-\\.\\^:,]","");
    }

    public void getAppVersion(){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);

            client.get(AplConstants.ServiceApp, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("result service app:", content);
                    try {
                        JSONObject response = new JSONObject(content);
                        String code = response.getString(AppParams.ERROR_CODE);
                        if (code.equals(AppParams.SUCCESS_CODE)) {
                            String arrayApp = response.getString(AppParams.APP_DATA);
                            final JSONObject mObject = new JSONObject(arrayApp);
                            String package_version = mObject.getString(AppParams.PACKAGE_VERSION);
                            final String package_name = mObject.getString(AppParams.PACKAGE_NAME);
                            final String type = mObject.getString(AppParams.TYPE);
                            String app_id = mObject.getString(AppParams.APP_ID);

                            if (!package_version.equalsIgnoreCase(BuildConfig.VERSION_NAME)) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityLogin.this)
                                        .setTitle("Update")
                                        .setMessage("Application is out of date,  Please update immediately")
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                final String appPackageName = package_name;
                                                if (type.equalsIgnoreCase("1")) {
                                                    try {
                                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                                    } catch (android.content.ActivityNotFoundException anfe) {
                                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                                    }
                                                } else if (type.equalsIgnoreCase("2")) {
                                                    String download_url = null;
                                                    try {
                                                        download_url = mObject.getString(AppParams.DOWNLOAD_URL);
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(download_url)));
                                                }
                                                ActivityLogin.this.finish();
                                                android.os.Process.killProcess(android.os.Process.myPid());
                                                System.exit(0);
                                                getParent().finish();
                                            }
                                        });
                                AlertDialog alertDialog = builder.create();
                                if (!isFinishing())
                                    alertDialog.show();
                            }
                        } else {
                            code = response.getString(AppParams.ERROR_MESSAGE);
                            Toast.makeText(getApplicationContext(), code, Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                ;

                public void onFailure(Throwable error, String content) {
                    Log.w("Error Koneksi get App Version", error.toString());
                    Toast.makeText(ActivityLogin.this, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }

                ;
            });

        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }

    private void findViews(){
        btnLogin    = (Button) findViewById(R.id.btnLogin);
        txtUserID   = (EditText) findViewById(R.id.txtUserID);
        txtPin      = (EditText) findViewById(R.id.inpPin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String userid = txtUserID.getText().toString();
                password = txtPin.getText().toString();

                if(userid.equals("") || password.equals("")) {
                    Toast.makeText(getApplicationContext(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                }
                else {
                    phone = editNoHP(userid);

                    pDialog = DefinedDialog.CreateProgressDialog(ActivityLogin.this, pDialog, "Logging In...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    RequestParams params = new RequestParams();

                    try {
                        password = Md5.hashMd5(password);
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }

                    params.put("user_id", phone);
                    params.put("password", password);
                    Log.d("params", params.toString());
                    OptGroupUserType = (RadioGroup) findViewById(R.id.OptGroupUserType);

                    // get selected radio button from radioGroup
                    int selectedId = OptGroupUserType.getCheckedRadioButtonId();

                    // find the radiobutton by returned id
                    radioLogin = (RadioButton) findViewById(selectedId);
                    final String loginType = radioLogin.getText().toString();
                    if (loginType.equalsIgnoreCase("Member / Agent")) {
                        Log.wtf("yang dihit", AplConstants.SignMobileAPI);
                        client.post(AplConstants.SignMobileAPI, params, new AsyncHttpResponseHandler() {
                            public void onSuccess(String content) {
                                Log.d("result:", content);
                                try {
                                    JSONObject object = new JSONObject(content);

                                    JSONObject req_param_data = object.getJSONObject("req_param_data");
                                    String rq_uuid = req_param_data.getString("rq_uuid");
                                    String rs_datetime = req_param_data.getString("rs_datetime");
                                    String service_id = req_param_data.getString("service_id");
                                    String decrypt_check = req_param_data.getString("decrypt_check");
                                    String error_code = req_param_data.getString("error_code");
                                    String error_message = req_param_data.getString("error_message");

                                    if (error_code.equals("0000")) {

                                        if (pDialog != null) {
                                            pDialog.dismiss();
                                        }

                                        JSONObject user_data = object.getJSONObject("user_data");

                                        String user_id = user_data.getString("user_id");
                                        String user_name = user_data.getString("user_name");
                                        String user_email = user_data.getString("user_email");
                                        String cust_id = user_data.getString("cust_id");


                                        SharedPreferences.Editor editor = getSharedPreferences("userinfo"
                                                , MODE_PRIVATE).edit();
                                        editor.putString("user_id", user_id);
                                        editor.putString("user_name", user_name);
                                        editor.putString("user_email", user_email);
                                        editor.putString("user_phone", phone);
                                        editor.putString("cust_id", cust_id);
                                        editor.putString("user_type", loginType);
                                        editor.commit();

                                        Intent intent = new Intent(ActivityLogin.this, MainActivity.class);
                                        startActivity(intent);
                                        finish();

                                    } else {
                                        if (pDialog != null) {
                                            pDialog.dismiss();
                                        }
                                        Toast.makeText(ActivityLogin.this, error_message, Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();

                                }
                            }

                            ;

                            public void onFailure(Throwable error, String content) {
                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }
                                Toast.makeText(getApplicationContext(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                            }

                            ;
                        });
                    } else {
                        Log.wtf("yang dihit", AplConstants.SignSalesMobileAPI);
                        client.post(AplConstants.SignSalesMobileAPI, params, new AsyncHttpResponseHandler() {
                            public void onSuccess(String content) {
                                Log.d("result:", content);
                                try {

                                    JSONObject object = new JSONObject(content);
                                    String error_code = object.getString("error_code");
                                    String error_message = object.getString("error_message");

                                    if (error_code.equals("0000")) {

                                        if (pDialog != null) {
                                            pDialog.dismiss();
                                        }

                                        String sales_id = object.getString("sales_id");
                                        String sales_name = object.getString("sales_name");
                                        String sales_code = object.getString("sales_code");

                                        SharedPreferences.Editor editor = getSharedPreferences("userinfo"
                                                , MODE_PRIVATE).edit();
                                        editor.putString("user_id", sales_id);
                                        editor.putString("user_name", sales_name);
                                        editor.putString("user_email", "");
                                        editor.putString("user_phone", phone);
                                        editor.putString("cust_id", sales_code);
                                        editor.putString("user_type", loginType);
                                        editor.commit();

                                        Intent intent = new Intent(ActivityLogin.this, MainActivity.class);
                                        startActivity(intent);
                                        finish();

                                    } else {
                                        if (pDialog != null) {
                                            pDialog.dismiss();
                                        }
                                        Toast.makeText(ActivityLogin.this, error_message, Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();

                                }
                            }

                            ;

                            public void onFailure(Throwable error, String content) {
                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }
                                Toast.makeText(getApplicationContext(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                            }

                            ;
                        });
                    }
                }
            }
        });
    }

}