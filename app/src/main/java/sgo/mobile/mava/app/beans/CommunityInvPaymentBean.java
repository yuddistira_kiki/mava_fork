package sgo.mobile.mava.app.beans;

public class CommunityInvPaymentBean {
    public String id     = "";
    public String code   = "";
    public String name   = "";
    public String sales_alias       = "";
    public String buss_scheme_code  = "";

    public CommunityInvPaymentBean(String id, String code, String name, String sales_alias, String buss_scheme_code) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.sales_alias = sales_alias;
        this.buss_scheme_code = buss_scheme_code;
    }

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSales_alias() {
        return sales_alias;
    }

    public void setSales_alias(String sales_alias) {
        this.sales_alias = sales_alias;
    }

    public String getBuss_scheme_code() {
        return buss_scheme_code;
    }

    public void setBuss_scheme_code(String buss_scheme_code) {
        this.buss_scheme_code = buss_scheme_code;
    }

    public String toString()
    {
        return( name  );
    }
}