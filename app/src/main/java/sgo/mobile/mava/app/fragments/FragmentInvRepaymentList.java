package sgo.mobile.mava.app.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.securepreferences.SecurePreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import in.srain.cube.views.ptr.header.StoreHouseHeader;
import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.adapter.InvoiceRepaymentAdapter;
import sgo.mobile.mava.app.beans.InvoiceRepaymentBean;
import sgo.mobile.mava.app.dialogs.DialogInvRepaymentDesc;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

/**
 * Created by thinkpad on 1/18/2016.
 */
public class FragmentInvRepaymentList extends Fragment {
    private ProgressDialog pDialog;
    Activity activity;
    SecurePreferences sp;
    FragmentManager fm;

    public String comm_id, comm_name, comm_code;

    // declare view objects
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert;
    TextView lbl_header;
    TableLayout tabel_footer;

    InvoiceRepaymentAdapter adapter;
    List<InvoiceRepaymentBean> listInvoice;

    Button btnDone;
    Button btnCancel;
    Button btnBack;

    // create price format
    DecimalFormat formatData = new DecimalFormat("#.##");

    //pull to refresh
    int page = 1;
    private PtrFrameLayout mPtrFrame;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_invrepayment_list, container, false);

        activity = getActivity();
        sp = new SecurePreferences(activity);
        listInvoice = new ArrayList<InvoiceRepaymentBean>();

        deleteInvoice();

        mPtrFrame = (PtrFrameLayout) view.findViewById(R.id.invRepayment_ptr_frame);
        txtAlert = (TextView) view.findViewById(R.id.txtAlert);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        listMenu   = (ListView) view.findViewById(R.id.list_invrepayment);
        tabel_footer = (TableLayout) view.findViewById(R.id.tabel_footer);

        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnCancel             = (Button) view.findViewById(R.id.btnCancel);
        btnBack               = (Button) view.findViewById(R.id.btnBack);

        Bundle bundle         = this.getArguments();
        comm_id               = bundle.getString(AppParams.COMM_ID);
        comm_name             = bundle.getString(AppParams.COMM_NAME);
        comm_code             = bundle.getString(AppParams.COMM_CODE);


        lbl_header = (TextView) view.findViewById(R.id.label_header);
        lbl_header.setText(getResources().getString(R.string.invoice_kmk));

        adapter = new InvoiceRepaymentAdapter(getActivity(), listInvoice, true);
        parseJSONData(page);

        StoreHouseHeader header = new StoreHouseHeader(getActivity().getApplicationContext());
        header.setPadding(0, 20, 0, 20);
        header.setTextColor(Color.BLACK);
        header.initWithString("Updating...");

        mPtrFrame.setDurationToCloseHeader(1500);
        mPtrFrame.setHeaderView(header);
        mPtrFrame.addPtrUIHandler(header);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                frame.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (txtAlert.getVisibility() == View.VISIBLE) {

                        } else {
                            page++;
                            parseJSONData(page);
                        }
                        mPtrFrame.refreshComplete();
                    }
                }, 1800);
            }

            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return !canScrollUp(((ListView) content)); // or cast with ListView
            }

            public boolean canScrollUp(View view) {
                if (android.os.Build.VERSION.SDK_INT < 14) {
                    if (view instanceof AbsListView) {
                        final AbsListView absListView = (AbsListView) view;
                        return absListView.getChildCount() > 0
                                && (absListView.getFirstVisiblePosition() > 0 || absListView
                                .getChildAt(0).getTop() < absListView.getPaddingTop());
                    } else {
                        return view.getScrollY() > 0;
                    }
                } else {
                    return ViewCompat.canScrollVertically(view, -1);
                }
            }
        });


        // event listener to handle list when clicked
        listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                FragmentManager fragmentManager = getFragmentManager();
                DialogInvRepaymentDesc dialog = new DialogInvRepaymentDesc();
                Bundle args = new Bundle();
                args.putString(AppParams.TX_ID, listInvoice.get(position).getTx_id());
                args.putString(AppParams.MAX_TX_DATE, listInvoice.get(position).getMax_tx_date());
                args.putString(AppParams.AMOUNT, listInvoice.get(position).getAmount());
                args.putString(AppParams.CCY_ID, listInvoice.get(position).getCcy_id());
                args.putString(AppParams.DESCRIPTION, listInvoice.get(position).getDesc());

                args.putString(AppParams.COMM_ID, comm_id);
                args.putString(AppParams.COMM_NAME, comm_name);
                args.putString(AppParams.COMM_CODE, comm_code);

                dialog.setArguments(args);
                getActivity().getSupportFragmentManager().popBackStack();

                dialog.setTargetFragment(FragmentInvRepaymentList.this, 100);
                dialog.show(fragmentManager, "dialogInvRepaymentDesc");
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                btnDone.setEnabled(false);
                btnDone.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        btnDone.setEnabled(true);
                    }
                }, 3000);

                String selected = "0";
                for(int i = 0 ; i < listInvoice.size() ; i++) {
                    selected = listInvoice.get(i).getSelected();
                    if(selected.equals("1")) break;
                }

                if(selected.equals("1")) {
                    Fragment newFragment = null;
                    newFragment = new FragmentPaymentRepayment();
                    Bundle args = new Bundle();
                    args.putString(AppParams.COMM_ID, comm_id);
                    args.putString(AppParams.COMM_NAME, comm_name);
                    args.putString(AppParams.COMM_CODE, comm_code);

                    newFragment.setArguments(args);
                    getActivity().getSupportFragmentManager().popBackStack();
                    switchFragment(newFragment);

                } else {
                    Toast.makeText(getActivity(), "Checkout tidak dapat dilakukan bila belum ada item yang dipilih!", Toast.LENGTH_SHORT).show();
                }
            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = null;
                newFragment = new FragmentInvRepaymentList();

                Bundle args = new Bundle();
                args.putString(AppParams.COMM_ID, comm_id);
                args.putString(AppParams.COMM_NAME, comm_name);
                args.putString(AppParams.COMM_CODE, comm_code);

                newFragment.setArguments(args);
                getActivity().getSupportFragmentManager().popBackStack();
                switchFragment(newFragment);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = null;
                newFragment = new FragmentCommunityRepayment();

                Bundle args = new Bundle();
                args.putString("title", getResources().getString(R.string.kmk));
                args.putString(AppParams.COMM_ID, comm_id);
                args.putString(AppParams.COMM_NAME, comm_name);
                args.putString(AppParams.COMM_CODE, comm_code);

                newFragment.setArguments(args);
                getActivity().getSupportFragmentManager().popBackStack();
                switchFragment(newFragment);
            }
        });

        return view;
    }

    public void parseJSONData(int _page){
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);

        String param_cust_id = AppHelper.getCustomerId(activity);

        RequestParams params = new RequestParams();
        params.put("cust_id", param_cust_id);
        params.put("comm_id", comm_id);
        params.put("page", Integer.toString(_page));
        params.put("limit", AppParams.LIMIT);

        Log.d("params", params.toString());
        client.post(AplConstants.InvKMKAPI, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {
                Log.d("result:", content);
                try {
                    // parse json data and store into arraylist variables
                    JSONObject json = new JSONObject(content);
                    String error_code = json.getString("error_code");
                    String error_message = json.getString("error_message");

                    if (error_code.equals(AppParams.SUCCESS_CODE)) {
                        List<InvoiceRepaymentBean> mListInvoice = new ArrayList<InvoiceRepaymentBean>();

                        JSONArray arrTransaction = json.getJSONArray("transaction"); // this is the "items: [ ] part

                        for (int i = 0; i < arrTransaction.length(); i++) {
                            String id = arrTransaction.getJSONObject(i).getString(AppParams.TX_ID);
                            boolean flagSame = false;

                            // cek apakah ada tx id yang sama.. kalau ada, tidak dimasukan ke array
                            if (listInvoice.size() > 0) {
                                for (InvoiceRepaymentBean aListInvoice : listInvoice) {
                                    if (!aListInvoice.getTx_id().equals(id)) {
                                        flagSame = false;
                                    } else {
                                        flagSame = true;
                                        break;
                                    }
                                }
                            }

                            if (!flagSame) {
                                JSONObject object = arrTransaction.getJSONObject(i);
                                InvoiceRepaymentBean invoice = new InvoiceRepaymentBean();
                                invoice.setTx_id(object.getString(AppParams.TX_ID));
                                invoice.setMax_tx_date(object.getString(AppParams.MAX_TX_DATE));
                                invoice.setAmount(object.getString(AppParams.AMOUNT));
                                invoice.setCcy_id(object.getString(AppParams.CCY_ID));
                                invoice.setDesc(object.getString(AppParams.DESCRIPTION));
                                invoice.setSelected("0");

                                mListInvoice.add(invoice);
                            }
                        }

                        // when finish parsing, hide progressbar
                        prgLoading.setVisibility(View.GONE);
                        insertInvoiceToDB(mListInvoice);

                    } else if(listInvoice.size() == 0){
                        prgLoading.setVisibility(View.GONE);
                        txtAlert.setVisibility(View.VISIBLE);
                        txtAlert.setText(error_message);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            }

            ;

            public void onFailure(Throwable error, String content) {
                Toast.makeText(activity, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }

            ;
        });
    }

    public void deleteInvoice(){
        ActiveAndroid.beginTransaction();
        listInvoice.clear();
        InvoiceRepaymentBean.deleteAll();
        ActiveAndroid.setTransactionSuccessful();
        ActiveAndroid.endTransaction();
    }

    public void insertInvoiceToDB(List<InvoiceRepaymentBean> mListInvoice) {
        ActiveAndroid.beginTransaction();
        InvoiceRepaymentBean mInv;

        Log.d("arrayPost length", String.valueOf(mListInvoice.size()));
        if (mListInvoice.size() > 0) {
            for (int i = 0; i < mListInvoice.size(); i++) {
                new InvoiceRepaymentBean();
                mInv = mListInvoice.get(i);
                mInv.save();
                Log.d("idx array posts", String.valueOf(i));
            }
        }

        ActiveAndroid.setTransactionSuccessful();
        ActiveAndroid.endTransaction();
        if(getActivity() != null){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    listInvoice.clear();
                    listInvoice.addAll(InvoiceRepaymentBean.getAll());

                    // if data available show data on list
                    // otherwise, show alert text
                    if (listInvoice.size() > 0) {
                        listMenu.setVisibility(View.VISIBLE);
                        listMenu.setAdapter(adapter);
                        lbl_header.setVisibility(View.VISIBLE);
                        tabel_footer.setVisibility(View.VISIBLE);
                    } else {
                        txtAlert.setVisibility(View.VISIBLE);
                    }
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(ActiveAndroid.inTransaction()){
            ActiveAndroid.endTransaction();
        }
        super.onDestroy();
    }
}
