package sgo.mobile.mava.frameworks.files;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.frameworks.text.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility functions to ease the work with files and file contents.
 *
 * @author Diki Irawan
 */

public class FileUtils {
    /** Counter for unfinished downloads */
    public static int openDownloads = 0;

    public static final String HTML_TYPE = "text/html";
    // mime-types
    public static final String PDF_TYPE = "application/pdf";

    public final static String FILE_EXTENSION_SEPARATOR = ".";
    /**
     * Fetches a document from a URL and writes the content to the given file.
     *
     * @param httpClient
     *            HTTP client used to fetch the document.
     * @param url
     *            The documents URL
     * @param targetFile
     *            The target file where the document's content should be written
     *            to
     *
     * @return The target file or null if error occurred.
     */
    public static File getFileFromURL(DefaultHttpClient httpClient, String url,
                                      File targetFile) {
        // check required variables
        if (url == null || httpClient == null) {
            return targetFile;
        }

        try {
            HttpGet request = new HttpGet(url);
            HttpResponse response = httpClient.execute(request);

            // download file from url
            HttpEntity entity = response.getEntity();
            InputStream in;
            in = entity.getContent();

            FileOutputStream fos;
            fos = new FileOutputStream(targetFile);

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                fos.write(buffer, 0, len1);
            }

            fos.close();

            return targetFile;

        } catch (/* ClientProtocolException, IOException */Exception e) {
            Log.e("FilegetFileFromURL", e.getMessage());
        }

        return null;
    }

    /**
     * Removes whitespaces and appends the file type.
     *
     * @param name
     *            The string that should be used as filename.
     * @param appendix
     *            The file type (e.g. ".pdf")
     *
     * @return The file name build from the arguments.
     */
    public static String getFilename(String name, String appendix) {
        return name.replace(" ", "_") + appendix;
    }

    /**
     * Returns a file on SD card. Creates it if not exists.
     *
     * @param folder
     *            The file's folder, relative to the cache directory.
     * @param filename
     *            The file's name.
     *
     * @return The file.
     * @throws Exception
     *             If SD-card does not exist
     */
    public static File getFileOnSD(String folder, String filename)
            throws Exception {
        // Save the file to/open from SD
        File path = new File(getCacheDir(folder));

        return new File(path, filename);
    }

    /**
     * Open a file with given mime-type using the ACTION_VIEW intent.
     *
     * @param file
     *            File to be opened.
     * @param context
     *            Activity calling.
     * @param mimeType
     *            The file's mime-type
     */
    public static void openFile(File file, Activity context, String mimeType) {
        if (file != null) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(file), mimeType);
            context.startActivity(intent);
        } else {
            Toast.makeText(context,
                    "Cannot open file",
                    Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Reads a text file and returns the content as a String.
     *
     * @param file
     *            The text file.
     * @return The file's content.
     */
    public static String readFile(File file) {
        StringBuilder contents = new StringBuilder();

        try {
            // use buffering, reading one line at a time
            // FileReader always assumes default encoding is OK!
            BufferedReader input = new BufferedReader(new FileReader(file));
            try {
                String line = null;
                while ((line = input.readLine()) != null) {
                    contents.append(line);
                    // contents.append(System.getProperty("line.separator"));
                }
            } finally {
                input.close();
            }
        } catch (IOException e) {
            Log.e("FilereadFile", e.getMessage());
        }

        return contents.toString();
    }

    /**
     * Gets a document from a URL and returns the source code as text.
     *
     * @param httpClient
     *            HTTP client used to fetch the document.
     * @param url
     *            The documents URL
     *
     * @return The document's source/the requests response
     */
    public static String sendGetRequest(DefaultHttpClient httpClient, String url) {
        return sendRequest(httpClient, new HttpGet(url));
    }

    /**
     * Sends a HTTP post request to given URL.
     *
     * @param httpClient
     *            The HTTP client.
     * @param url
     *            The request URL.
     */
    public static String sendPostRequest(DefaultHttpClient httpClient,
                                         String url) {
        return sendRequest(httpClient, new HttpPost(url));
    }

    /**
     * Sends a HTTP request.
     *
     * @param httpClient
     *            The corresponding HTTP client.
     * @param request
     *            The request to be send.
     * @return The response as String.
     */
    public static String sendRequest(DefaultHttpClient httpClient,
                                     HttpRequestBase request) {
        HttpResponse response;
        String respContent = "";

        try {
            response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                respContent = EntityUtils.toString(entity);
                // Log.d("RESP", "content: " + respContent);
                entity.consumeContent();
            }
        } catch (Exception /* ClientProtocolException, IOException */e) {
            Log.e("FileUtils.sendRequest", e.getMessage());
        }
        return respContent;
    }

    /**
     * Writes a String to a text file.
     *
     * @param file
     *            The target file.
     *
     * @param content
     *            The text content to be written.
     */
    public static void writeFile(File file, String content) {
        InputStream in;
        in = new ByteArrayInputStream(content.getBytes());

        FileOutputStream fos;
        try {
            fos = new FileOutputStream(file);

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                fos.write(buffer, 0, len1);
            }

            fos.close();
        } catch (Exception e) {
            Log.e("FilewriteFile", e.getMessage());
        }
    }

    /**
     * Deletes all contents of a cache directory
     *
     * <pre>
     * @param directory directory postfix (e.g. feeds/cache)
     * </pre>
     */
    public static void emptyCacheDir(String directory) {
        // TODO Who handles exception?
        try {
            File dir = new File(getCacheDir(directory));
            if (dir.isDirectory() && dir.canWrite()) {
                for (String child : dir.list()) {
                    new File(dir, child).delete();
                }
            }
        } catch (Exception e) {
            log(e, directory);
        }
    }

    /**
     * Returns the full path of a cache directory and checks if it is readable
     * and writable
     *
     * <pre>
     * @param directory directory postfix (e.g. feeds/cache)
     * @return full path of the cache directory
     * @throws Exception
     * </pre>
     */
    // TODO Think how not to hardcode Exception text.
    public static String getCacheDir(String directory) throws IOException {
        File f = new File(Environment.getExternalStorageDirectory().getPath()
                + "/sgo/" + directory);
        if (!f.exists()) {
            f.mkdirs();
        }
        if (!f.canRead()) {
            throw new IOException("Cannot read from SD-Card");
        }
        if (!f.canWrite()) {
            throw new IOException("Cannot write to SD-Card");
        }
        return f.getPath() + "/";
    }

    /**
     * Logs an exception and additional information
     *
     * <pre>
     * @param e Exception (source for message and stacktrace)
     * @param message Additional information for exception message
     * </pre>
     */
    public static void log(Exception e, String message) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        Log.e("SGO : ", e + " " + message + "\n" + sw.toString());
    }

    /**
     * Logs a message
     *
     * <pre>
     * @param message Information or Debug message
     * </pre>
     */
    public static void log(String message) {
        StackTraceElement s = Thread.currentThread().getStackTrace()[3];
        Log.d("SGO : ", s.toString() + " " + message);
    }

    /**
     * Download a file in the same thread
     *
     * <pre>
     * @param url Download location
     * @param target Target filename in local file system
     * @throws Exception
     * </pre>
     */
    private static void downloadFile(String url, String target)
            throws Exception {
        File f = new File(target);
        if (f.exists()) {
            return;
        }
        HttpClient httpclient = new DefaultHttpClient();
        HttpEntity entity = httpclient.execute(new HttpGet(url)).getEntity();

        if (entity == null) {
            return;
        }
        File file = new File(target);
        InputStream in = entity.getContent();

        FileOutputStream out = new FileOutputStream(file);
        byte[] buffer = new byte[8192];
        int count = -1;
        while ((count = in.read(buffer)) != -1) {
            out.write(buffer, 0, count);
        }
        out.flush();
        out.close();
        in.close();
    }

    /**
     * Download a file in a new thread
     *
     * <pre>
     * @param url Download location
     * @param target Target filename in local file system
     * </pre>
     */
    public static void downloadFileThread(final String url, final String target) {
        openDownloads++;
        new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Who handles exception?
                try {
                    log(url);
                    downloadFile(url, target);
                    openDownloads--;
                } catch (Exception e) {
                    log(e, url);
                }
            }
        }).start();
    }

    /**
     * Download an icon in the same thread
     *
     * <pre>
     * @param url Download location
     * @param target Target filename in local file system
     * @throws Exception
     * </pre>
     */
    private static void downloadIconFile(String url, String target)
            throws Exception {
        File f = new File(target);
        if (f.exists()) {
            return;
        }
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);

        // force mobile version of a web page
        httpget.addHeader("User-Agent",
                "Mozilla/5.0 (iPhone; de-de) AppleWebKit/528.18 Safari/528.16");

        HttpEntity entity = httpclient.execute(httpget).getEntity();
        if (entity == null) {
            return;
        }
        InputStream in = entity.getContent();
        String data = convertStreamToString(in);

        String icon = "";
        Pattern link = Pattern.compile("<link[^>]+>");
        Pattern href = Pattern.compile("href=[\"'](.+?)[\"']");

        Matcher matcher = link.matcher(data);
        while (matcher.find()) {
            String match = matcher.group(0);

            Matcher href_match = href.matcher(match);
            if (href_match.find()) {
                if (match.contains("shortcut icon") && icon.length() == 0) {
                    icon = href_match.group(1);
                }
                if (match.contains("apple-touch-icon")) {
                    icon = href_match.group(1);
                }
            }
        }

        Uri uri = Uri.parse(url);
        // icon not found
        if (icon.length() == 0) {
            icon = "http://" + uri.getHost() + "/favicon.ico";
        }
        // relative url
        if (!icon.contains("://")) {
            icon = "http://" + uri.getHost() + "/" + icon;
        }
        // download icon
        downloadFile(icon, target);
    }

    /**
     * Convert an input stream to a string
     *
     * <pre>
     * @param is input stream from file, download
     * @return output string
     * </pre>
     */
    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        // TODO Who handles exception?
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }


    /**
     * Download an icon in a new thread
     *
     * <pre>
     * @param url Download location
     * @param target Target filename in local file system
     * </pre>
     */
    public static void downloadIconFileThread(final String url,
                                              final String target) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Who handles exception?
                try {
                    log(url);
                    downloadIconFile(url, target);
                } catch (Exception e) {
                    log(e, url);
                }
            }
        }).start();
    }

    /**
     *
     * @param url
     * @param target
     */
    public static void downloadImageAndCompressThread(final String url,
                                                      final String target, final String targetImageThumbnail) {
        openDownloads++;
        new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Who handles exception?
                try {
                    log(url);
                    downloadFile(url, target);
                    openDownloads--;

                    try {
                        Bitmap sourceImage = BitmapFactory.decodeFile(target);
                        Bitmap thumbail = Bitmap.createScaledBitmap(
                                sourceImage, 200, 200, false);

                        FileOutputStream out = new FileOutputStream(
                                targetImageThumbnail);
                        thumbail.compress(Bitmap.CompressFormat.JPEG, 60, out);
                        out.flush();
                        out.close();
                    } catch (Exception e) {
                        Log.w("Gallery", "Error scaling image");
                    }
                } catch (Exception e) {
                    log(e, url);
                }
            }
        }).start();
    }

    /**
     * Download a JSON stream from a URL
     *
     * <pre>
     * @param url Valid URL
     * @return JSONObject
     * @throws Exception
     * </pre>
     */
    public static JSONObject downloadJson(String url) throws Exception {
        log(url);

        HttpClient httpclient = new DefaultHttpClient();
        HttpParams params = httpclient.getParams();
        HttpConnectionParams.setSoTimeout(params, AplConstants.HTTP_TIMEOUT);
        HttpConnectionParams.setConnectionTimeout(params, AplConstants.HTTP_TIMEOUT);

        HttpEntity entity = null;
        try {
            entity = httpclient.execute(new HttpGet(url)).getEntity();
        } catch (Exception e) {
            // Throw a new TimeputException which is treted later
            throw new TimeoutException("HTTP Timeout");
        }

        String data = "";
        if (entity != null) {

            // JSON Response Read
            InputStream instream = entity.getContent();
            data = convertStreamToString(instream);

            log(data);
            instream.close();
        }
        return new JSONObject(data);
    }

    /**
     * Download a String from a URL
     *
     * <pre>
     * @param url Valid URL
     * @return String
     * @throws Exception
     * </pre>
     */
    public static String downloadString(String url) throws Exception {
        log(url);

        HttpClient httpclient = new DefaultHttpClient();
        HttpEntity entity = httpclient.execute(new HttpGet(url)).getEntity();
        if (entity == null) {
            return "";
        }
        String data = EntityUtils.toString(entity);
        log(data);
        return data;
    }

    /**
     * read file
     *
     * @param filePath
     * @param charsetName The name of a supported {@link java.nio.charset.Charset </code>charset<code>}
     * @return if file not exist, return null, else return content of file
     * @throws java.io.IOException if an error occurs while operator BufferedReader
     */
    public static StringBuilder readFile(String filePath, String charsetName) {
        File file = new File(filePath);
        StringBuilder fileContent = new StringBuilder("");
        if (file == null || !file.isFile()) {
            return null;
        }

        BufferedReader reader = null;
        try {
            InputStreamReader is = new InputStreamReader(new FileInputStream(file), charsetName);
            reader = new BufferedReader(is);
            String line = null;
            while ((line = reader.readLine()) != null) {
                if (!fileContent.toString().equals("")) {
                    fileContent.append("\r\n");
                }
                fileContent.append(line);
            }
            reader.close();
            return fileContent;
        } catch (IOException e) {
            throw new RuntimeException("IOException occurred. ", e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    throw new RuntimeException("IOException occurred. ", e);
                }
            }
        }
    }

    /**
     * write file
     *
     * @param filePath
     * @param content
     * @param append is append, if true, write to the end of file, else clear content of file and write into it
     * @return return true
     * @throws java.io.IOException if an error occurs while operator FileWriter
     */
    public static boolean writeFile(String filePath, String content, boolean append) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(filePath, append);
            fileWriter.write(content);
            fileWriter.close();
            return true;
        } catch (IOException e) {
            throw new RuntimeException("IOException occurred. ", e);
        } finally {
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    throw new RuntimeException("IOException occurred. ", e);
                }
            }
        }
    }

    /**
     * write file
     *
     * @param filePath
     * @param stream
     * @return return true
     * @throws java.io.IOException if an error occurs while operator FileWriter
     */
    public static boolean writeFile(String filePath, InputStream stream) {
        OutputStream o = null;
        try {
            o = new FileOutputStream(filePath);
            byte data[] = new byte[1024];
            int length = -1;
            while ((length = stream.read(data)) != -1) {
                o.write(data, 0, length);
            }
            o.flush();
            return true;
        } catch (FileNotFoundException e) {
            throw new RuntimeException("FileNotFoundException occurred. ", e);
        } catch (IOException e) {
            throw new RuntimeException("IOException occurred. ", e);
        } finally {
            if (o != null) {
                try {
                    o.close();
                    stream.close();
                } catch (IOException e) {
                    throw new RuntimeException("IOException occurred. ", e);
                }
            }
        }
    }

    /**
     * read file to string list, a element of list is a line
     *
     * @param filePath
     * @param charsetName The name of a supported {@link java.nio.charset.Charset </code>charset<code>}
     * @return if file not exist, return null, else return content of file
     * @throws java.io.IOException if an error occurs while operator BufferedReader
     */
    public static List<String> readFileToList(String filePath, String charsetName) {
        File file = new File(filePath);
        List<String> fileContent = new ArrayList<String>();
        if (file == null || !file.isFile()) {
            return null;
        }

        BufferedReader reader = null;
        try {
            InputStreamReader is = new InputStreamReader(new FileInputStream(file), charsetName);
            reader = new BufferedReader(is);
            String line = null;
            while ((line = reader.readLine()) != null) {
                fileContent.add(line);
            }
            reader.close();
            return fileContent;
        } catch (IOException e) {
            throw new RuntimeException("IOException occurred. ", e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    throw new RuntimeException("IOException occurred. ", e);
                }
            }
        }
    }

    /**
     * get file name from path, not include suffix
     *
     * <pre>
     *      getFileNameWithoutExtension(null)               =   null
     *      getFileNameWithoutExtension("")                 =   ""
     *      getFileNameWithoutExtension("   ")              =   "   "
     *      getFileNameWithoutExtension("abc")              =   "abc"
     *      getFileNameWithoutExtension("a.mp3")            =   "a"
     *      getFileNameWithoutExtension("a.b.rmvb")         =   "a.b"
     *      getFileNameWithoutExtension("c:\\")              =   ""
     *      getFileNameWithoutExtension("c:\\a")             =   "a"
     *      getFileNameWithoutExtension("c:\\a.b")           =   "a"
     *      getFileNameWithoutExtension("c:a.txt\\a")        =   "a"
     *      getFileNameWithoutExtension("/home/admin")      =   "admin"
     *      getFileNameWithoutExtension("/home/admin/a.txt/b.mp3")  =   "b"
     * </pre>
     *
     * @param filePath
     * @return file name from path, not include suffix
     * @see
     */
    public static String getFileNameWithoutExtension(String filePath) {
        if (StringUtils.isEmpty(filePath)) {
            return filePath;
        }

        int extenPosi = filePath.lastIndexOf(FILE_EXTENSION_SEPARATOR);
        int filePosi = filePath.lastIndexOf(File.separator);
        if (filePosi == -1) {
            return (extenPosi == -1 ? filePath : filePath.substring(0, extenPosi));
        }
        if (extenPosi == -1) {
            return filePath.substring(filePosi + 1);
        }
        return (filePosi < extenPosi ? filePath.substring(filePosi + 1, extenPosi) : filePath.substring(filePosi + 1));
    }

    /**
     * get file name from path, include suffix
     *
     * <pre>
     *      getFileName(null)               =   null
     *      getFileName("")                 =   ""
     *      getFileName("   ")              =   "   "
     *      getFileName("a.mp3")            =   "a.mp3"
     *      getFileName("a.b.rmvb")         =   "a.b.rmvb"
     *      getFileName("abc")              =   "abc"
     *      getFileName("c:\\")              =   ""
     *      getFileName("c:\\a")             =   "a"
     *      getFileName("c:\\a.b")           =   "a.b"
     *      getFileName("c:a.txt\\a")        =   "a"
     *      getFileName("/home/admin")      =   "admin"
     *      getFileName("/home/admin/a.txt/b.mp3")  =   "b.mp3"
     * </pre>
     *
     * @param filePath
     * @return file name from path, include suffix
     */
    public static String getFileName(String filePath) {
        if (StringUtils.isEmpty(filePath)) {
            return filePath;
        }

        int filePosi = filePath.lastIndexOf(File.separator);
        return (filePosi == -1) ? filePath : filePath.substring(filePosi + 1);
    }

    /**
     * get folder name from path
     *
     * <pre>
     *      getFolderName(null)               =   null
     *      getFolderName("")                 =   ""
     *      getFolderName("   ")              =   ""
     *      getFolderName("a.mp3")            =   ""
     *      getFolderName("a.b.rmvb")         =   ""
     *      getFolderName("abc")              =   ""
     *      getFolderName("c:\\")              =   "c:"
     *      getFolderName("c:\\a")             =   "c:"
     *      getFolderName("c:\\a.b")           =   "c:"
     *      getFolderName("c:a.txt\\a")        =   "c:a.txt"
     *      getFolderName("c:a\\b\\c\\d.txt")    =   "c:a\\b\\c"
     *      getFolderName("/home/admin")      =   "/home"
     *      getFolderName("/home/admin/a.txt/b.mp3")  =   "/home/admin/a.txt"
     * </pre>
     *
     * @param filePath
     * @return
     */
    public static String getFolderName(String filePath) {

        if (StringUtils.isEmpty(filePath)) {
            return filePath;
        }

        int filePosi = filePath.lastIndexOf(File.separator);
        return (filePosi == -1) ? "" : filePath.substring(0, filePosi);
    }

    /**
     * get suffix of file from path
     *
     * <pre>
     *      getFileExtension(null)               =   ""
     *      getFileExtension("")                 =   ""
     *      getFileExtension("   ")              =   "   "
     *      getFileExtension("a.mp3")            =   "mp3"
     *      getFileExtension("a.b.rmvb")         =   "rmvb"
     *      getFileExtension("abc")              =   ""
     *      getFileExtension("c:\\")              =   ""
     *      getFileExtension("c:\\a")             =   ""
     *      getFileExtension("c:\\a.b")           =   "b"
     *      getFileExtension("c:a.txt\\a")        =   ""
     *      getFileExtension("/home/admin")      =   ""
     *      getFileExtension("/home/admin/a.txt/b")  =   ""
     *      getFileExtension("/home/admin/a.txt/b.mp3")  =   "mp3"
     * </pre>
     *
     * @param filePath
     * @return
     */
    public static String getFileExtension(String filePath) {
        if (StringUtils.isBlank(filePath)) {
            return filePath;
        }

        int extenPosi = filePath.lastIndexOf(FILE_EXTENSION_SEPARATOR);
        int filePosi = filePath.lastIndexOf(File.separator);
        if (extenPosi == -1) {
            return "";
        }
        return (filePosi >= extenPosi) ? "" : filePath.substring(extenPosi + 1);
    }

    /**
     * Creates the directory named by the trailing filename of this file, including the complete directory path required
     * to create this directory. <br/>
     * <br/>
     * <ul>
     * <strong>Attentions:</strong>
     * <li>makeDirs("C:\\Users\\Trinea") can only create users folder</li>
     * <li>makeFolder("C:\\Users\\Trinea\\") can create Trinea folder</li>
     * </ul>
     *
     * @param filePath
     * @return true if the necessary directories have been created or the target directory already exists, false one of
     * the directories can not be created.
     * <ul>
     * <li>if {@link FileUtils#getFolderName(String)} return null, return false</li>
     * <li>if target directory already exists, return true</li>
     * <li>return {@link java.io.File#makeFolder}</li>
     * </ul>
     */
    public static boolean makeDirs(String filePath) {
        String folderName = getFolderName(filePath);
        if (StringUtils.isEmpty(folderName)) {
            return false;
        }

        File folder = new File(folderName);
        return (folder.exists() && folder.isDirectory()) ? true : folder.mkdirs();
    }

    /**
     * @param filePath
     * @return
     * @see #makeDirs(String)
     */
    public static boolean makeFolders(String filePath) {
        return makeDirs(filePath);
    }

    /**
     * Indicates if this file represents a file on the underlying file system.
     *
     * @param filePath
     * @return
     */
    public static boolean isFileExist(String filePath) {
        if (StringUtils.isBlank(filePath)) {
            return false;
        }

        File file = new File(filePath);
        return (file.exists() && file.isFile());
    }

    /**
     * Indicates if this file represents a directory on the underlying file system.
     *
     * @param directoryPath
     * @return
     */
    public static boolean isFolderExist(String directoryPath) {
        if (StringUtils.isBlank(directoryPath)) {
            return false;
        }

        File dire = new File(directoryPath);
        return (dire.exists() && dire.isDirectory());
    }

    /**
     * delete file or directory
     * <ul>
     * <li>if path is null or empty, return true</li>
     * <li>if path not exist, return true</li>
     * <li>if path exist, delete recursion. return true</li>
     * <ul>
     *
     * @param path
     * @return
     */
    public static boolean deleteFile(String path) {
        if (StringUtils.isBlank(path)) {
            return true;
        }

        File file = new File(path);
        if (!file.exists()) {
            return true;
        }
        if (file.isFile()) {
            return file.delete();
        }
        if (!file.isDirectory()) {
            return false;
        }
        for (File f : file.listFiles()) {
            if (f.isFile()) {
                f.delete();
            } else if (f.isDirectory()) {
                deleteFile(f.getAbsolutePath());
            }
        }
        return file.delete();
    }

    /**
     * get file size
     * <ul>
     * <li>if path is null or empty, return -1</li>
     * <li>if path exist and it is a file, return file size, else return -1</li>
     * <ul>
     *
     * @param path
     * @return
     */
    public static long getFileSize(String path) {
        if (StringUtils.isBlank(path)) {
            return -1;
        }

        File file = new File(path);
        return (file.exists() && file.isFile() ? file.length() : -1);
    }

    public static String fileName(String path) {
        if (!StringUtils.isBlank(path)) {
            if (path.endsWith("/")) {
                path = path.substring(0, path.length() - 1);
            }
            int ind = path.lastIndexOf('/');
            return path.substring(ind + 1, path.length());
        }
        return "";
    }

    public static String parentPath(String path) {
        if (StringUtils.isBlank(path) || path.equals("/")) {
            return "";
        } else {
            if (path.endsWith("/")) {
                path = path.substring(0, path.length() - 1);
            }
            int ind = path.lastIndexOf('/');
            return path.substring(0, ind + 1);
        }
    }

}
