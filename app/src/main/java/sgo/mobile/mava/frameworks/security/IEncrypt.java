package sgo.mobile.mava.frameworks.security;

public interface IEncrypt {
    public String encrypt(String str);
}
