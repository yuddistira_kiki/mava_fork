package sgo.mobile.mava.conf;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AplConstants {

    public static Boolean IS_PROD = true;
    public static final int HTTP_DEFAULT_TIMEOUT = 60 * 1000;
    public static final int HTTP_LONG_TIMEOUT = 240 * 1000;
    public static final String CallbackURL = "https://www.goworld.asia/";

    public static final String headaddress = "https://mobile.espay.id/kwsg/";

    public static final String SignSalesMobileAPI  = headaddress + "SalesLogin/SignIn";
    public static final String SalesForgotPassMobileAPI    = headaddress + "SalesForgotPassword/Invoke";
    public static final String SalesChangePassMobileAPI    = headaddress + "SalesChangePassword/Invoke";
    public static final String DiCommunityMobileAPI  = headaddress + "DigInclusionCommunity/Invoke";
    public static final String DiMemberMobileAPI  = headaddress + "DigInclusionMember/Retrieve";
    public static final String DiListMobileAPI  = headaddress + "DigInclusionList/Retrieve";
    public static final String DiListPayMobileAPI  = headaddress + "DigInclusionList/RetrievePay";
    public static final String DiSavePayMobileAPI  = headaddress + "DigInclusionList/Invoke";
    public static final String DiValidateMobilAPI = headaddress + "DigInclusionList/Validate";
    public static final String DiPaymentMobilAPI = headaddress + "DigInclusionPayment/Validate";
    public static final String CommunityInvMobileAPI  = headaddress + "InvCommunity/Invoke";
    public static final String MemberInvMobileAPI     = headaddress + "InvMember/Retrieve";
    public static final String InvListMobileAPI     = headaddress + "InvList/Retrieve";
    public static final String InvListPayMobileAPI  = headaddress + "InvList/RetrievePay";
    public static final String InvValidateMobilAPI = headaddress + "InvList/Validate";
    public static final String InvSavePayMobileAPI  = headaddress + "InvList/Invoke";
    public static final String ReqTokenInvPaymentMobileAPI  = headaddress + "ReqTokenInv/Invoke";
    public static final String ConfirmTokenInvPaymentMobileAPI  = headaddress + "ConfirmTokenInv/Invoke";
    public static final String PaymentReportInvPaymentMobileAPI    = headaddress + "InvPayment/Invoke";
    public static final String CommunityInvUserMobileAPI  = headaddress + "DgiPaymentCommunity/Retrieve";
    public static final String MemberInvUserMobileAPI     = headaddress + "DgiPaymentMember/Retrieve";
    public static final String InvUserListMobileAPI     = headaddress + "DgiPaymentInv/Retrieve";
    public static final String InvUserListPayMobileAPI  = headaddress + "InvListUser/RetrievePay";
    public static final String InvValidatUsereMobilAPI = headaddress + "DgiPaymentInv/Validate";
    public static final String InvUserSavePayMobileAPI  = headaddress + "DgiPaymentInv/Invoke";
    public static final String PaymentReportInvUserMobileAPI    = headaddress + "InvPaymentUser/Invoke";
    public static final String BankCCLMobileAPI  = headaddress + "CashCollectionBank/Retrieve";
    public static final String CommunityCCLMobileAPI  = headaddress + "CommunityCCL/Retrieve";
    public static final String MemberCCLMobileAPI     = headaddress + "CashCollectionMember/Retrieve";
    public static final String MemberCCLDIMobileAPI     = headaddress + "DiCCLMember/Retrieve";
    public static final String ConfirmTokenDiMobileAPI  = headaddress + "ConfirmTokenDigInclusion/Invoke";
    public static final String ResendTokenDiMobileAPI  = headaddress + "ResendTokenDigInclusion/Invoke";
    public static final String ValidateCCLMobileAPI      = headaddress + "CashCollectionValidate/Invoke";
    public static final String PaymentReportCCLMobileAPI      = headaddress + "CashCollectionPayment/Invoke";
    public static final String DepositCCLMobileAPI      = headaddress + "ReqTokenCashCollection/Invoke";
    public static final String RequestTokenCCLMobileAPI    = headaddress + "ReqTokenCashCollection/Invoke";
    public static final String ConfirmTokenCCLMobileAPI    = headaddress + "ConfirmTokenCashCollection/Invoke";
    public static final String DiReportCommunity    = headaddress + "DiReportCommunity/Retrieve";
    public static final String DiReportMaster    = headaddress + "DiReportMaster/Retrieve";
    public static final String DiReportDetail    = headaddress + "DiReportDetail/Retrieve";
    public static final String ListMenuLCSMobileAPI = headaddress + "ServiceMenuSetting/GetMenuSettingLCS";
    public static final String ForgotPassMobileAPI    = headaddress + "MemberForgotPassword/Invoke";
    public static final String ChangePassMobileAPI    = headaddress + "MemberChangePassword/Invoke";
    public static final String CommunityDiCCLMobileAPI     = headaddress + "DiCCLCommunity/Retrieve";
    public static final String DiCCLListMobileAPI     = headaddress + "DiCCLList/Retrieve";
    public static final String RequestTokenCCLDIMobileAPI    = headaddress + "ReqTokenDiCCL/Invoke";
    public static final String ConfirmTokenCCLDIMobileAPI    = headaddress + "ConfirmTokenDiCCL/Invoke";
    public static final String RequestTokenCCLDIRandomMobileAPI    = headaddress + "ReqTokenDiCCLRandom/Invoke";
    public static final String ConfirmTokenCCLDIRandomMobileAPI    = headaddress + "ConfirmTokenDiCCLRandom/Invoke";
    public static final String ValidateCCLDIMobileAPI      = headaddress + "DiCCLValidate/Invoke";
    public static final String PaymentReportCCLDIMobileAPI      = headaddress + "DigInclusionPayment/Invoke";
    public static final String SignMobileAPI  = headaddress + "MemberLogin/SignIn";
    public static final String PulsaMobileAPI = headaddress + "Transaction/telco";
    public static final String TopUpMobileAPI = headaddress + "Transaction/TopUp";
    public static final String CashInC2CMobileAPI          = headaddress + "Cash2Cash/CashIn";
    public static final String ConfirmTokenC2CMobileAPI    = headaddress + "Cash2Cash/ConfirmToken";
    public static final String CashOutC2CMobileAPI         = headaddress + "Cash2Cash/CashOut";
    public static final String DepositC2AMobileAPI      = headaddress + "Cash2Account/Deposit";
    public static final String ConfirmTokenC2AMobileAPI    = headaddress + "Cash2Account/ConfirmToken";
    public static final String DenomMobileAPI = headaddress + "Transaction/Denom";
    public static final String RequestTokenEw9      = headaddress + "ReqTokenEw9/Invoke";
    public static final String ConfirmTokenEw9      = headaddress + "ConfirmTokenEw9/Invoke";
    public static final String ListInvoiceMobileAPI = headaddress + "ServiceListInvoices/Retrieve";
    public static final String PayInvoiceMobileAPI  = headaddress + "ServicePayInvoice/Invoke";
    public static final String TokenInvoiceMobileAPI = headaddress + "TokenInvoice/Invoke";
    public static final String ReqTokenInvoiceMobileAPI = headaddress + "ReqTokenSms/Invoke";
    public static final String ListAmountTrxMobileAPI = headaddress + "ServiceAmountTrx/Retrieve";
    public static final String ListMenuSettingMobileAPI = headaddress + "ServiceMenuSetting/GetAllMenuSetting";
    public static final String CommunityListMobileAPI = headaddress + "ServiceCommunity/Retrieve";
    public static final String MemberListMobileAPI = headaddress + "ServiceMember/Retrieve";
    public static final String BankListMobileAPI = headaddress + "ServiceBank/Retrieve";
    public static final String BankProductMobileAPI = headaddress + "ServiceBank/BankProduct";
    public static final String InquiryTopUpMobileAPI  = headaddress + "InquiryTopUp/Retrieve";
    public static final String TopUpMemberMobileAPI   = headaddress + "TopUpMember/Retrieve";
    public static final String TopUpPaymentMobileAPI  = headaddress + "TopUpPayment/Invoke";
    public static final String TopUpValidateMobileAPI = headaddress + "TopUpValidate/Invoke";
    public static final String ServiceApp = headaddress + "ServiceApp/getAppVersion";
    public static final String DGIPaymentAPI = headaddress + "DgiPayment/Invoke";
    public static final String TrxStatusAPI = headaddress + "TrxStatus/Retrieve";
    public static final String InquiryTrxAPI = headaddress + "InquiryTrx/Retrieve";
    public static final String InsertTrxAPI = headaddress + "InsertTrx/Invoke";
    public static final String PaymentMemberAPI = headaddress + "PaymentMember/Invoke";
    public static final String CommunityKMKAPI = headaddress + "CommunityKMK/Retrieve";
    public static final String BankKMKAPI = headaddress + "BankKMK/Retrieve";
    public static final String InvKMKAPI = headaddress + "InvKMK/Retrieve";
    public static final String RepayInvKMKAPI = headaddress + "RepayInvKMK/Invoke";
    public static final String InquiryTrxKMKAPI = headaddress + "InquiryTrxKMK/Retrieve";
    public static final String ReqTokenKMKAPI = headaddress + "ReqTokenKMK/Invoke";
    public static final String ConfirmTokenKMKAPI = headaddress + "ConfirmTokenKMK/Invoke";

    public static final String AcctMandiriKUM = headaddress + "AcctMandiriKUM/Retrieve";
    public static final String LimitMandiriKUM = headaddress + "LimitMandiriKUM/Retrieve";
    public static final String DiReportEspay = headaddress + "DiReportEspay/Retrieve";
    public static final String LINK_INQUIRY_SMS = headaddress + "InquirySMS/Retrieve";
    //#################################################################################################################//

    public static final String APP_ID = "SMGR";
    //public static final String CommCodeCTA   =  "CTA0003";
    public static final String CommCodeCTA   =  "CTASGO6";
    //public static final String MemberCodeCTA =  "CTA1";
    public static final String MemberCodeCTA =  "ESPCash2Account";

    public static final String CommCodeCTC   =  "CTCSGO6";
    //public static final String CommCodeCTC   =  "CTCALFA";
    public static final String MemberCodeCTC =  "ESPCash2Cash2";
    //public static final String MemberCodeCTC =  "CTCALFA002";

    public static final String CommCodeCCL   = "CCL";
    public static final String MemberCodeCCL = "NORASEVELCCL";
    public static final String SchemeCodeCCL = "CCL";
    public static final String SenderIdCCL   = "MOBILE";

    public static String INCOMINGSMS_SPRINT = "+6281333332000";

    //#################################################################################################################//

    public static final String SalesIdInv  = "RAMLANGUST1392238684Y6MMN";
    public static final String MemberIdInv = "123";
    public static final String AreaInv     = "Jakarta";

    //public static final String NoIncomingToken = "6281382555096";
    public static final String NoIncomingToken = "6281514407650";
    //------------------------------------------------------------------------------------------------------------------//

    public static final int HTTP_TIMEOUT = 20 * 1000;

    public static final String MIN_AMOUNT_PAY = "1";
    public static final String MAX_AMOUNT_PAY = "2000000000";

    public AplConstants(){}


    public static String getCurrentDate(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", new Locale("id","INDONESIA"));
        return df.format(Calendar.getInstance().getTime());
    }

    public static String getCurrentDate(int minus){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", new Locale("id","INDONESIA"));
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -minus);
        return df.format(calendar.getTime());
    }
}
